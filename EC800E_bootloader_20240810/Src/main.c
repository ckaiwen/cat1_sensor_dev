#include "stm32l0xx_hal.h"
#include "sys.h"
#include "usart.h"
#include "delay.h"
#include "stm_flash.h"
#include "iap.h"


#include "EC800.h"
#include "http.h"
#include "App.h"

#include "TIM.h"


int main(void)
{   
 // 	HAL_Init(); 
	//  SystemClock_Config_8M();//配置时钟，工作频率为8mhz
	//  delay_ms(200);//延时让系统电压稳定 
	  
	  LPUART1_Init(9600); //调试串口初始化
	  printf("enter iap\r\n");
		EC800_Config();//Cat1模块初始化
	  
	  int8_t ret = App_Init();
	  uint8_t err_count=0;
	  if(ret!=0){   //需要更新
			while(1){
			   ret =App_Update();
				 if(ret == -1){ //更新出错  
						err_count+=1;
					  if(err_count>=3){
						 	printf("update err\r\n");
							Cat1_Power_Off();

							 __HAL_RCC_USART2_CLK_DISABLE();
							__HAL_RCC_LPUART1_CLK_DISABLE();
							GPIO_InitTypeDef GPIO_Initure={0};
							GPIO_Initure.Pin=GPIO_PIN_9|GPIO_PIN_10;            //PA9，PA10
							GPIO_Initure.Mode=GPIO_MODE_ANALOG;     //模拟
							GPIO_Initure.Pull=GPIO_NOPULL;          //不带上下拉
							HAL_GPIO_Init(GPIOA,&GPIO_Initure);
							 
							for(uint16_t i=0;i<=3600;i++){
							   delay_ms(1000);
							} 
	          	HAL_NVIC_SystemReset();
							
						}
				 }else{
				    break;
				 }
		 }
	 }
		printf("jump app\r\n");
	  delay_ms(200);
		IAP_Load_App(APP_Flash_Addr);	
	
}
