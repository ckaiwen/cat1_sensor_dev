#include "usart.h"

UART_HandleTypeDef LPUART1_Handler; //LPUART1 句柄
UART_HandleTypeDef USART2_Handler; //USART2 句柄


Com2_Struct com2_struct; //串口2的结构体
Com2_Struct *com2=&com2_struct; //串口2的结构体


//LPUART1配置
//bound:波特率
void LPUART1_Init(uint32_t bound)
{  
     GPIO_InitTypeDef GPIO_InitStruct={0};
    //开启时钟
    __HAL_RCC_GPIOA_CLK_ENABLE();           //使能GPIOA时钟
    __HAL_RCC_LPUART1_CLK_ENABLE();         //使能LPUART1时钟
    
   //IO设置
		GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF6_LPUART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		
	//UART 初始化设置
    LPUART1_Handler.Instance=LPUART1;                       //LPUART1
    LPUART1_Handler.Init.BaudRate=bound;                    //波特率
    LPUART1_Handler.Init.WordLength=UART_WORDLENGTH_8B;     //字长为8位数据格式
    LPUART1_Handler.Init.StopBits=UART_STOPBITS_1;          //一个停止位
    LPUART1_Handler.Init.Parity=UART_PARITY_NONE;           //无奇偶校验位
    LPUART1_Handler.Init.HwFlowCtl=UART_HWCONTROL_NONE;     //无硬件流控
   // LPUART1_Handler.Init.Mode=UART_MODE_TX_RX;              //收发模式
		LPUART1_Handler.Init.Mode=UART_MODE_TX;              //收发模式
		HAL_UART_Init(&LPUART1_Handler);                //HAL_UART_Init()会使能LPUART1
  //中断配置		
//   HAL_NVIC_SetPriority(LPUART1_IRQn,0,1); //抢占优先级0，子优先级1
//	 HAL_NVIC_EnableIRQ(LPUART1_IRQn);       //使能USART1中断通道
//  __HAL_UART_ENABLE_IT(&LPUART1_Handler,UART_IT_RXNE);  //开启接收完成中断
//	__HAL_UART_ENABLE_IT(&LPUART1_Handler,UART_IT_ORE);  //开启接收完成中断
}


//USART2配置
//bound:波特率
void USART2_Init(uint32_t bound)
{  
     GPIO_InitTypeDef GPIO_InitStruct={0};
    //开启时钟
 //   __HAL_RCC_GPIOA_CLK_ENABLE();           //使能GPIOA时钟
    __HAL_RCC_USART2_CLK_ENABLE();         //使能USART2时钟
    
   //IO设置
		GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_USART2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		
	//UART 初始化设置
    USART2_Handler.Instance=USART2;                       //UART2
    USART2_Handler.Init.BaudRate=bound;                    //波特率
    USART2_Handler.Init.WordLength=UART_WORDLENGTH_8B;     //字长为8位数据格式
    USART2_Handler.Init.StopBits=UART_STOPBITS_1;          //一个停止位
    USART2_Handler.Init.Parity=UART_PARITY_NONE;           //无奇偶校验位
    USART2_Handler.Init.HwFlowCtl=UART_HWCONTROL_NONE;     //无硬件流控
    USART2_Handler.Init.Mode=UART_MODE_TX_RX;              //收发模式
		HAL_UART_Init(&USART2_Handler);                //HAL_UART_Init()会使能USART2
		
  //中断配置		
    HAL_NVIC_SetPriority(USART2_IRQn,0,0); //抢占优先级1，子优先级0
		HAL_NVIC_EnableIRQ(USART2_IRQn);       //使能USART2中断通道
		
		__HAL_UART_ENABLE_IT(&USART2_Handler,UART_IT_RXNE);  //开启接收完成中断
		__HAL_UART_ENABLE_IT(&USART2_Handler,UART_IT_ORE);  //开启接收完成中断
}


//串口2中断服务程序
void USART2_IRQHandler(void)
{   
	  uint8_t temp;
    if((__HAL_UART_GET_FLAG(&USART2_Handler,UART_FLAG_RXNE)!=RESET))//接收中断
    {  
			 temp=USART2->RDR;
			 
			 if(com2->rx_len< (COM2_Rec_Max_Len-2)){  
				  com2->rx_buf[com2->rx_len+1]='\0';
			    com2->rx_buf[com2->rx_len]=temp;
			    com2->rx_len+=1;
			 }
    }
    HAL_UART_IRQHandler(&USART2_Handler);
}



/*串口2发送
buf需发送数据的首地址
len需发送数据的长度*/
void  Usart2Send(uint8_t *buf, uint16_t len)
{   
	 HAL_UART_Transmit(&USART2_Handler,buf,len,1000);
	 
}

int fputc(int ch, FILE *f)      //重定义fputc函数
{   
    while((LPUART1->ISR&0X40)==0){};//循环发送,直到发送完毕
    LPUART1->TDR = (uint8_t) ch;
    return ch;
}




