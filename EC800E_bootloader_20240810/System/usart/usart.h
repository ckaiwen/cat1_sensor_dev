#ifndef __USART_H
#define __USART_H
#include "stm32l0xx.h"
#include "stdio.h"	
#include "App.h"


#define COM2_Rec_Max_Len  (2*1024+128)/*COM2接收长度*/

/*串口2结构体*/
typedef struct{
	 uint8_t rx_buf[COM2_Rec_Max_Len]; //接收的缓冲区
	 uint16_t rx_len;
}Com2_Struct;

//对外变量
extern Com2_Struct *com2; //串口2

//对外函数
void LPUART1_Init(uint32_t bound); //串口1配置

void USART2_Init(uint32_t bound); //串口2配置
void Usart2Send(uint8_t *buf, uint16_t len); //串口2发送数据
#endif
