#include "stm_flash.h"

/*Flash整页页读出
page为0-255
data读数据的缓存,大小为128*/
void Flash_Page_Read(uint8_t page,uint8_t *data)
{  
	 uint32_t addr = FLASH_BASE + page*FLASH_PAGE_SIZE;
   for(uint32_t i=0;i<128;i++)
	{
	   *(data) = *(uint8_t *)(addr);
		 data++;
		 addr++;
	}
}


/*Flash页写入
page为0-255
data需要写入的地址,长度为128
返回错误码HAL_StatusTypeDef*/
//int8_t  Flash_Page_Write(uint8_t page,uint8_t *data)
//{  
//	 uint32_t err;
//	 uint32_t addr = FLASH_BASE + page*FLASH_PAGE_SIZE;
//	 
//   FLASH_EraseInitTypeDef EraseInitStruct;
//	 EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
//	 EraseInitStruct.PageAddress=addr;
//	 EraseInitStruct.NbPages = 1;
//	 
//	 HAL_FLASH_Unlock();
//	 if(HAL_FLASHEx_Erase(&EraseInitStruct, &err) !=HAL_OK) //擦除页
//	 {
//	    return -1;
//	 }
//	 
//	 uint32_t dat32;
//	 for(uint8_t i=0;i<32;i++)
//	 { 
//		 dat32 = *(uint32_t*)data;
//	   err = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, addr, dat32);
//	   if(err != HAL_OK)
//	   {
//	      return -1;
//	   }
//		 addr += 4;
//		 data +=4;
//	 }
//	 HAL_FLASH_Lock();
//	 return 0;
//}


/*Flash写入
page写入的开始页为0-255
data需要写入的地址
len是写入数据的长度
返回错误码HAL_StatusTypeDef*/
int8_t  Flash_Write(uint8_t page,uint8_t *data,uint32_t len)
{  
	 uint32_t err;
	 uint32_t addr = FLASH_BASE + page*FLASH_PAGE_SIZE;
	
   FLASH_EraseInitTypeDef EraseInitStruct;
	 EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	 EraseInitStruct.PageAddress=addr;
	 EraseInitStruct.NbPages = len / FLASH_PAGE_SIZE;
	 if((len%FLASH_PAGE_SIZE)>0)
	 {
	     EraseInitStruct.NbPages +=1;
	 }
	 
	 HAL_FLASH_Unlock();
	 if(HAL_FLASHEx_Erase(&EraseInitStruct, &err) !=HAL_OK) //擦除页
	 {
	    return -1;
	 }
	 
	 uint32_t dat32;
	 uint32_t len32 = len/4;
	 if((len%4)>0)
	 {
	    len32+=1;
	 }
	 for(uint32_t i=0;i<len32;i++)
	 { 
		 dat32 = (*(data+3)<<24) + (*(data+2)<<16) + (*(data+1)<<8) + *(data);
	   err = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, addr, dat32);
	   if(err != HAL_OK)
	   {
	      return -1;
	   }
		 addr += 4;
		 data +=4;
	 }
	 HAL_FLASH_Lock();
	 return 0;
}



