#include "iap.h" 

pFunction jump2app; 


//跳转到应用程序段
//appxaddr:用户代码起始地址.

void IAP_Load_App(uint32_t appxaddr)
{ 
	 uint32_t JumpAddress;
	if(((*(vu32*)appxaddr)&0x2FFE0000)==0x20000000)	//检查栈顶地址是否合法.
	{ 
		JumpAddress = *(volatile uint32_t*)(appxaddr + 4);
    jump2app = (pFunction)JumpAddress;
		
		MSR_MSP(*(vu32*)appxaddr);					//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
		INTX_DISABLE();  //关闭所有中断
		 __set_CONTROL(0);
		jump2app();									//跳转到APP.
	}
}		 





