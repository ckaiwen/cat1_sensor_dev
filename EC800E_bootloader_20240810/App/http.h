#ifndef __HTTP_H
#define __HTTP_H
#include "stm32l0xx.h"
#include "EC800.h"
#include "App.h"


int8_t AT_QHTTPURL(char *url);//设置要返回的URL
int8_t AT_QHTTPGET(void);//发送 HTTP GET 请求，最大响应时间为80秒
int8_t AT_QHTTPREAD(uint32_t *res_addr);//读取HTTP响应信息并通过UART口将其输出
int8_t Http_Get(char *url,uint32_t *res_addr);//http get请求
#endif


