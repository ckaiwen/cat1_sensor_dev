#include "HTTP.h"


/*设置要返回的URL*/
int8_t AT_QHTTPURL(char *url)
{
  char cmd[128]={0};
	uint16_t len = sprintf(cmd,"AT+QHTTPURL=%d,80\r\n",strlen(url));
	if(Cat1_Send(cmd,len,"CONNECT",5000)==NULL){
	   return -1;
	}
	
	if(Cat1_Send(url,strlen(url),"OK",5000)==NULL){ 
	   return -1;
	}
	return 0;
}

//发送 HTTP GET 请求，最大响应时间为60秒
int8_t AT_QHTTPGET()
{
	 char *cmd="AT+QHTTPGET=60\r\n";
	 if(Cat1_Send(cmd,16,"+QHTTPGET",12000)==NULL){
			 return -1;
	 }
	 return 0;
}

/*QHTTPREAD反馈发送,由于接收中代码片段是字节，无法用strstr判断
 *buf为发送的内容
 *len为发送的长度
 *out_time为等待返回的超时时间
*/
char* QHTTPREAD_Send(char* s,uint16_t len,char *sub_s,uint16_t out_time)
{    
	   char *recv=NULL;
	   uint16_t idx=0;
	   uint16_t have_rec_time=0; 
	   
		 Usart2Send((uint8_t*)s,len);
	   com2->rx_len=0;
		 printf("send:%s",(char*)s);	
	    
		 //等待接收完成
		 while(1)
		 {  
			  if(have_rec_time < out_time){
					 if(com2->rx_len>20 && com2->rx_buf[com2->rx_len-2]==0x0D && com2->rx_buf[com2->rx_len-1]==0x0A){
						  if(strstr((char*)&com2->rx_buf[com2->rx_len-12],sub_s)!=NULL){   //接收到正确的字符串
						     recv=(char*)com2->rx_buf;
						     break;
							}else if(strstr((char*)com2->rx_buf,"ERROR")!=NULL){   //接收到错误的字符串
						     break;
							}
					 }
				}
				else 
				{
				    break;
				}
				delay_ms(1);
				have_rec_time+=1;
		 }
		 //打印数据
		 if(com2->rx_len>=4){
 			 if(com2->rx_buf[0]==0x0D && com2->rx_buf[1]==0x0A){
			    idx=2;
			 }
       printf("recv:%s\r\n",(char*)&com2->rx_buf[idx]);		 
		 }
		
		 return recv;
}

//读取HTTP响应信息并通过UART口将其输出
int8_t AT_QHTTPREAD(uint32_t *res_addr)
{
   char *cmd="AT+QHTTPREAD=60\r\n";
	 char *str= QHTTPREAD_Send(cmd,17,"READ",12000); 
	 if(str == NULL)
	 {
			 return -1;
	 }
	 *res_addr =(uint32_t) str;
	 return 0;
}

/*http get请求*/
int8_t Http_Get(char *url,uint32_t *res_addr)
{
   if(AT_QHTTPURL(url)!=0)
	{
	   return -1;
	}
	delay_ms(1000);
	if(AT_QHTTPGET()!=0)
	{
	   return -1;
	}
	delay_ms(1000);
	if(AT_QHTTPREAD(res_addr)!=0)
	{
	    return -1;
	}
	delay_ms(1000);
	return 0;
}

