#ifndef __APP_H
#define __APP_H
#include "stm32l0xx.h"
#include "stdlib.h"
#include "stdio.h"
#include "delay.h"
#include "usart.h"
#include "iap.h" 
#include "stm_flash.h"
#include "EC800.h"
#include "HTTP.h"

//陈工的地址
//#define IP   "http://cat.nongyemeta.com:9200"
//#define Firmware_Info_Url "http://cat.nongyemeta.com:9200/api/v1/iap/get_firmware_info?version="
//#define Get_Firmware_Url  "http://cat.nongyemeta.com:9200/api/v1/iap/get_firmware?version="

//APE平台产品ID，用于区分不同的客户
//#define APE_Product_Id  "17004186"  //陈工 ape平台上mqtt产品的id
#define APE_Product_Id  "17040095"  //王总ape平台上mqtt产品的id,17040095

//王总的地址
#define IP   "http://39.101.136.74:9200"
#define Firmware_Info_Url "http://39.101.136.74:9200/api/v1/iap/get_firmware_info?version="
#define Get_Firmware_Url  "http://39.101.136.74:9200/api/v1/iap/get_firmware?version="

#define Cat1_Max_Err_Count   5  //AT命令发给Cat1，返回错误或者不返回次数超过等于该次数，Cat1模块重启一次

#define APP_Page        64   //8K地址开始
#define APP_Flash_Addr  (0x08000000 + APP_Page*FLASH_PAGE_SIZE)
#define Data_Page   255  //数据存储的page
#define Pack_Len    2048 //一包数据的长度
#define Version_Len 20 //版本号的最大长度
#define Update_Success_Flag 0x55 //更新成功的标志
#define Need_Update_Fix_Version  0xAA //需要更新设定的版本
#define Need_Check_Update  0x77 //需要app确认是否更新成功
//如果其他的是默认更新最新版本

typedef struct
{  
	uint8_t upate_flag; //是否更新最新版本
	char update_version[Version_Len]; //需要更新的版本
	uint32_t size;//更新版本的字节数
}APP_Struct;


extern APP_Struct *app;

uint8_t App_Init(void);
int App_Update(void);

static int8_t Get_App_Info(uint32_t* size,char *version);
static int8_t Get_App_Data(uint32_t addr,uint32_t len,uint32_t *data_addr);
int8_t App_Info_Save(void);
#endif


