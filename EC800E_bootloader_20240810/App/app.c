#include "App.h"

APP_Struct  app_struct1;
APP_Struct  *app = &app_struct1;


/*返回0不需要更新，返回1需要更新*/
uint8_t App_Init()
{
   uint8_t buf[128];
	 uint8_t i=0;
	 
	 Flash_Page_Read(Data_Page,buf);
	 
	 app->upate_flag =buf[0];
	 if(app->upate_flag==Update_Success_Flag) //不需要更新
	 {    
		  return 0;
	 }
	 else if(app->upate_flag==Need_Update_Fix_Version)
	 {
			 for(;i<(Version_Len-1);i++)//获取更新的版本号
			 {
				 app->update_version[i] = buf[1+i];
			 }
		}
	 app->update_version[i]='\0';
		
	 return 1;
}


int8_t Get_App_Info(uint32_t* size,char *version)
{
  char url[128]={0};
	uint32_t res_addr;
	
	sprintf(url,"%s%s&productId=%s",Firmware_Info_Url,app->update_version,APE_Product_Id);
	if(Http_Get(url,&res_addr)!=0)
	{
	   return -1;
	}
	
	char *str1,*str2 ;
	str1= strstr((char*)(res_addr),"size"); //size":1556,"version":"20240105"}
	if(str1==NULL)
	{
	   return -1;
	}

	str1 = (str1+6);//1556,"version":"20240105"}
	str2 = strstr(str1,":");//:"20240105"}
	if(str2==NULL)
	{
	   return -1;
	}
	
	char *str_temp= strstr(str1,",");
	*str_temp='\0';
	*size = atoi(str1);
	
	uint8_t i=0;
	for(;i<(Version_Len-1);i++)
	{  
		 if(str2[2+i]=='"')
		 {
		    break;
		 }
	   version[i] = str2[2+i];
	}
	version[i]='\0';
	return 0;
}

int8_t Get_App_Data(uint32_t addr,uint32_t len,uint32_t *data_addr)
{
  char url[128]={0};
	uint32_t res_addr;
	sprintf(url,"%s%s&productId=%s&addr=%d&size=%d",Get_Firmware_Url,app->update_version,APE_Product_Id,addr,len);
	
	if(Http_Get(url,&res_addr)!=0)
	{
	   return -1;
	}
	
	char *str = (char*)res_addr;
  str = strstr(str,"CONNECT");
  if(str==NULL)
	{
	  return -1;
	}		
	*data_addr =(uint32_t)(str+9);
	return 0;
}

int8_t App_Info_Save()
{
   uint8_t buf[32]={0};
	 buf[0]=Need_Check_Update; //更新完后赋值Need_Check_Update，如果在app正常运行起来,就赋值为更新成功
	 for(uint8_t i=0;i<20;i++)
	 {
	    buf[1+i]= app->update_version[i];
	 }
	 return Flash_Write(Data_Page,buf,32);
}

int App_Update()
{
    int8_t ret=Get_App_Info(&app->size,app->update_version);
    if(ret!=0)
		{  
		   return -1;
		}
	
		uint32_t current_pack_len;
		uint32_t have_update_size = 0x00;
		uint32_t data_addr =0x00;
		while(1)
		{
		   if((app->size - have_update_size)> Pack_Len)
			 {
			    current_pack_len = Pack_Len;
			 }
			 else
			 {
			    current_pack_len = app->size - have_update_size;
			 }
			 
			 //获取数据
			 ret =Get_App_Data(have_update_size, current_pack_len,&data_addr);
			 if(ret !=0)
			 {  		  
			    return -1;
			 } 
		 
			 //写入flash
			 ret =  Flash_Write(APP_Page+have_update_size/128,(uint8_t*)data_addr,current_pack_len);
			 if(ret !=0)
			 {  
			    return -1;
			 } 
			 
			 have_update_size += current_pack_len;
			 if(have_update_size>= app->size) //写入完毕
			 {
			    break;
			 }
		}
		
		//保存版本信息
		ret = App_Info_Save(); 
		if(ret !=0)
		{
			 return -1;
		}
		return 0;
}








