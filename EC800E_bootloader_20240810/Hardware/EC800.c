#include "EC800.h"


void Cat1_PWR_On()
{
    GPIO_InitTypeDef GPIO_Initure;
	  GPIO_Initure.Mode=GPIO_MODE_OUTPUT_OD;  //浮空输出
	  GPIO_Initure.Speed=GPIO_SPEED_LOW; 		//低速
	  GPIO_Initure.Pin=GPIO_PIN_6;  //PB6
		HAL_GPIO_Init(GPIOB,&GPIO_Initure);
	
	 // HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET);
	  GPIOB->BRR = GPIO_PIN_6;	 
}

void Cat1_PWR_Off()
{
    GPIO_InitTypeDef GPIO_Initure;
		GPIO_Initure.Mode=GPIO_MODE_INPUT;  //输入
		GPIO_Initure.Pull=GPIO_NOPULL; 			//没上下拉
		GPIO_Initure.Speed=GPIO_SPEED_LOW; 		//低速
	  GPIO_Initure.Pin=GPIO_PIN_6;  //PB6
	  HAL_GPIO_Init(GPIOB,&GPIO_Initure);
}

void EC800_Config()
{
		GPIO_InitTypeDef GPIO_Initure;
    
	//  __HAL_RCC_GPIOA_CLK_ENABLE(); //开启 GPIOA 时钟
		__HAL_RCC_GPIOB_CLK_ENABLE(); //开启 GPIOB 时钟
	
	  GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
	  GPIO_Initure.Speed=GPIO_SPEED_LOW; 		//低速
		
		//L76K
	 GPIO_Initure.Pin = GPIO_PIN_0;
   HAL_GPIO_Init(GPIOA, &GPIO_Initure);
	 GPIOA->BRR = GPIO_PIN_0;
	
	  //PWRKEY，在开机状态下拉低PWRKEY至少650ms后释放关机
	  GPIO_Initure.Pin=GPIO_PIN_15;  //PA15
		HAL_GPIO_Init(GPIOA,&GPIO_Initure);
    
	  //DTR,通过主机拉低模块 MAIN_DTR可唤醒模块
	  GPIO_Initure.Pin=GPIO_PIN_1;  //PB1
		HAL_GPIO_Init(GPIOB,&GPIO_Initure);
	  
	  //输入
		GPIO_Initure.Mode=GPIO_MODE_INPUT;  //输入
		GPIO_Initure.Pull=GPIO_NOPULL; 			//没上下拉
		GPIO_Initure.Speed=GPIO_SPEED_LOW; 		//低速
		
		//STATUS,当模块正常开机时，STATUS 输出高电平
	  GPIO_Initure.Pin=GPIO_PIN_8;  //PA8
		HAL_GPIO_Init(GPIOA,&GPIO_Initure);
		


		Cat1_DTR_High();
		Cat1_PWRKEY_High();
	  Cat1_PWR_Off();
		delay_ms(200);
		
	  EC800_Set_Boud();
	  Cat1_AT_ATE(); 	 
}



/*由于单片机主频低，不支持115200通讯,把EC800通讯波特率设置为9600*/
void EC800_Set_Boud()
{  
		uint8_t err_time=0;
		//判断发AT命令是否成功,成功直接返回
		while(1)
		{
		   if(Cat1_AT_At()==0){
				  return;
			}			
			 err_time+=1;
			 if(err_time>=2){
					break;
			 }
			 delay_ms(2000);	
		}
		
		//提高单片机主频，设置cat1模块串口波特率为9600
	//	SystemClock_Config_8M();
		USART2_Init(115200); //Cat1模块串口初始化
	//	LPUART1_Init(9600);
		delay_ms(100);
		Cat1_AT_Set_Baud_9600();
		//printf("set cat boud 9600\r\n");
		delay_ms(200);
		HAL_NVIC_SystemReset();	 	
}


/*开机*/
int8_t Cat1_Power_On()
{  
   if(Cat1_Get_Status()!=GPIO_PIN_RESET){ //已经在开机中  
	    return 1;
	 }
	  printf("cat1 start on\r\n");
	 //开机初始化
	 Cat1_PWRKEY_Low();
	 Cat1_PWR_On(); 
	 USART2_Init(9600);
	 com2->rx_len=0;
	 
	 //判断开机信号
	 uint16_t wait_time=0;
	 while(wait_time<= Power_On_Time )
	 { 
		 if(strstr((char*)com2->rx_buf,"RDY")!=NULL || Cat1_Get_Status()!=GPIO_PIN_RESET){ //status状态已经在开机中
			   delay_ms(2000); 
				 printf("cat1 on success\r\n");
				 return 0;  
		 }
		 delay_ms(1);
		 wait_time+=1;
	 }
	 
//	err:
//	USART2_DeInit();
  Cat1_PWRKEY_High();
	Cat1_PWR_Off(); 
	printf("cat1 on fail\r\n");	
	delay_ms(500); 
	return -1;		 
}


/*关机,已经关机中返回1，开机成功返回0,关机失败返回-1*/
void Cat1_Power_Off()
{ 
//   if(Cat1_Get_Status()==0){ //已经在关机中  	
//	    return ;
//	 }
	// delay_ms(100);
	 //Cat1_AT_QPOWD();
	 Cat1_Send("AT+QPOWD=0\r\n",12,"DOWN",5000);
	 //减少代码尺寸
//	 uint16_t wait_time=0;
//   while(wait_time<Power_Off_Time) //开机成功
//	 {
//		    if(Cat1_Get_Status()==0){
//	         printf("cat1 poweroff success\r\n");	
//					 delay_ms(100);
//					 goto end;
//				}
//				delay_ms(1);
//				wait_time+=1;
//	 }
//	 
//	 printf("cat1 poweroff fail，force poweroff\r\n");
//	 
//	 end:
	 delay_ms(Power_Off_Time);
//   USART2_DeInit();
   Cat1_PWR_Off(); 
	// delay_ms(500);
	 printf("cat1 off\r\n");	
	 return ;
}


/*Cat1模块反馈发送
 *buf为发送的内容
 *len为发送的长度
 *out_time为等待返回的超时时间
*/
uint8_t cat1_err_count=0;
char* Cat1_Send(char* s,uint16_t len,char *sub_s,uint16_t out_time)
{    
	   char *recv=NULL; //接收到的字符串
	   uint16_t have_rec_time=0; //已经接收时间
	   
	   //开机
	   if(Cat1_Power_On()==-1){ //开机失败
		    return NULL;
		 }
		  //唤醒
	   Cat1_DTR_Low();
	   delay_ms(10);
	   
		 //发送
		 Usart2Send((uint8_t*)s,len);
	   com2->rx_len=0;
		 printf("send:%s",(char*)s);	
	    
		 //等待接收完成
		 while(1)
		 {  
			  if(have_rec_time < out_time){
					 if(com2->rx_len>=6 && com2->rx_buf[com2->rx_len-2]==0x0D && com2->rx_buf[com2->rx_len-1]==0x0A){
						  if(strstr((char*)com2->rx_buf,sub_s)!=NULL){   //接收到正确的字符串
						     recv=(char*)com2->rx_buf;
						     break;
							}else if(strstr((char*)com2->rx_buf,"ERROR")!=NULL){   //接收到错误的字符串
						     break;
							}
					 }
				}
				else {   
				    break;
				}
				delay_ms(1);
				have_rec_time+=1;
		 }
		 
		 //打印数据
		   uint8_t idx=0;
		 if(com2->rx_len>=4){
 			 if(com2->rx_buf[0]==0x0D && com2->rx_buf[1]==0x0A){
			    idx=2;
			 }
       printf("recv:%s\r\n",(char*)&com2->rx_buf[idx]);		 
		 }
		 
		 //记录错误次数
		 if(recv==NULL){
		    cat1_err_count+=1;
		 }else{
		   cat1_err_count=0;
		 }
		 
		  //错误次数过多，关机重启
	   if(cat1_err_count >=Cat1_Max_Err_Count){
			  cat1_err_count=0;
		    Cat1_Power_Off();
			 	delay_ms(200);
		 }
		 
		 Cat1_DTR_High(); //拉高进入休眠
		 return recv;
}


//AT命令,判断模块通信是否正常
int8_t Cat1_AT_At()
{  
   if(Cat1_Send("AT\r\n",4,"OK",300)==NULL) {
	     return -1;
	 }
	 return 0;
}

//AT命令设置波特率
int8_t Cat1_AT_Set_Baud_9600()
{  
	 if(Cat1_Send("AT+IPR=9600;&W\r\n",16,"OK",800)== NULL ) {
	     return -1;
	 }
	 return 0;
}


//设置不返回发送消息
int8_t Cat1_AT_ATE()
{
	 if(Cat1_Send("ATE0\r\n",6,"OK",300)==NULL){
	     return -1;
	 }
	//减少代码尺寸
	 delay_ms(10);
	 if(Cat1_Send("AT&W\r\n",6,"OK",300)== NULL){
	     return -1;
	 }
	 return 0;
}

//立即关机命令
int8_t Cat1_AT_QPOWD()
{  
   if(Cat1_Send("AT+QPOWD=0\r\n",12,"DOWN",5000)==NULL) {
	     return -1;
	 }
	 return 0;
}






