#ifndef __EC800_H
#define __EC800_H
#include "stm32l0xx.h"
#include "usart.h"
#include <string.h>


#define Power_On_Time  16000
#define Power_Off_Time  8000


#define Cat1_PWRKEY_High()   GPIOA->BSRR = GPIO_PIN_15	    //PA15置1
#define Cat1_PWRKEY_Low()    GPIOA->BRR  = GPIO_PIN_15	  //PA15置0


#define Cat1_DTR_High()      GPIOB->BSRR = GPIO_PIN_1	    //PB1置1
#define Cat1_DTR_Low()       GPIOB->BRR = GPIO_PIN_1	  //PB1置0

#define Cat1_Get_Status()    (GPIOA->IDR & GPIO_PIN_8)

void EC800_Config(void);//EC800配置
int8_t Cat1_Power_On(void);//开机
void Cat1_Power_Off(void);//关机

char* Cat1_Send(char* s,uint16_t len,char *sub_s,uint16_t out_time);//Cat1模块反馈发送数据


int8_t Cat1_AT_At(void);//AT命令,判断模块通信是否正常
int8_t Cat1_AT_Set_Baud_9600(void);
int8_t Cat1_AT_ATE(void); //设置串口发送数据不回显
int8_t Cat1_AT_QPOWD(void); //关机



//内部函数
static void EC800_Set_Boud(void);
static void EC800_AT_Init(void);//AT相关命令初始化
#endif
