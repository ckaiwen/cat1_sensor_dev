#ifndef __Update_H
#define __Update_H

#include "stm32l0xx.h"
#include "stdlib.h"
#include <string.h>
#include "stm_flash.h"
#include "EC800.h"

#define Data_Page   255  //数据存储的page
#define Pack_Len    2048 //一包数据的长度
#define Version_Len 20 //版本号的最大长度
#define Update_Success_Flag 0x55 //更新成功的标志
#define Need_Update_Fix_Version  0xAA //需要更新设定的版本
#define Need_Check_Update  0x77 //需要app确认是否更新成功

typedef struct
{  
	uint8_t upate_flag; //是否更新最新版本
	char update_version[Version_Len]; //需要更新的版本
}Update_Struct;

extern Update_Struct  *update;

void Update_Init(void);
void A99_Cmd_Hander(char *s);
int8_t App_Info_Save(void);
#endif


