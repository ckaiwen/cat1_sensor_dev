#include "App.h"

APP_Struct  app_struct1;
APP_Struct  *app = &app_struct1;

//APP启动
void App_Start()  
{
   //默认A0模式
//	 app->a1_sleep_state = 0;
//	 app->a0_noraml_state = 1 ;
//	 app->a0_need_pub_flag = 1;
	 
	 //默认A1模式
	 app->a1_sleep_state = 1;
	 app->a0_noraml_state = 0;
	 app->a0_need_pub_flag = 0;
	 app->c0_location_state = 0;
 
	 app->need_pub_c6 = 1;
	 com2->rx_len=0;
}


/*BDS处理*/
void BDS_Dev_Handle()
{  
	 static uint8_t gps_pwr_state=0;
	 static uint32_t bds_time=0; //获取到bds的时间
	 static uint32_t bds_get_start_time=0;
	 //不在a0模式或在C4模式失能休眠
   if(app->a0_noraml_state !=1){ //确保关闭bds
     app->bds_need_get_flag=0;
  }	 
	 
	if(L76k->bds_get_flag != 0){//获取到定位信息后，600S之内不再获取
	   if((app->base_time - bds_time)< GPS_Keep_Time){
		    app->bds_need_get_flag=0;
		 }else{
		    L76k->bds_get_flag=0;
		 }
	}

	 if(app->bds_need_get_flag == 0){//不需要获取bds就关闭
		  com1->rx_len=0;
		  com1->rx_state=0;
      L76K_Off() ;
		 
		  gps_pwr_state=0;
		  return;
	 }
	 
	 if(gps_pwr_state==0){
	    bds_get_start_time=app->base_time;
		  gps_pwr_state=1;
		  printf("gps get start\r\n");
	 }
		L76K_On();
	  if((app->base_time- bds_get_start_time)> Get_BDS_Time){ //300S获取不到GPS，就不再获取了
		   L76k->bds_get_flag = 2;
			 bds_time= app->base_time;
			 printf("gps get fail\r\n");
		}else{
				if(com1->rx_state==1){ //bds解析
					if(L76K_Data_Decode((char*)com1->rx_buf)==0){ //获取gps成功
						 bds_time= app->base_time;
						 L76k->bds_get_flag=1;
						printf("gps:%s,date:%s\r\n",com1->rx_buf,L76k->date);
					}
					com1->rx_state=0;
				}
		}
}


/*运动量7段分类*/
static void motion_sort(uint32_t *addr,uint16_t motion)
{
    if(motion< 150){
		   addr[0]+=1;
		}else if(motion< 300){
		   addr[1]+=1;
		}else if(motion< 450){
		   addr[2]+=1;
		}else if(motion< 600){
		   addr[3]+=1;
		}else if(motion< 750){
		   addr[4]+=1;
		}else if(motion< 900){
		   addr[5]+=1;
		}else{
		   addr[6]+=1;
		}
}

//mpu调用放到定时器里面*/
uint16_t x,y,z; //读取的数据
int x1,y1,z1;//当前转成加速度,单位为mg的数据
int x0,y0,z0;//上次加速度数据,单位为mg的数据
uint16_t currentMotion;//当前这次的运动量


uint32_t all_motion[All_Motion_Selection_Num]={0};//累计的运动量
uint8_t  all_motion_idx=0; //累计运动量采集的存放的索引
uint32_t all_motion_count=0;//累计运动量采集次数


uint32_t motion_hour=0;//运动量采集的时间段,0-3
uint32_t motion_count=0;//运动量当前采集次数
uint8_t  current_free_fall_count=0;

uint32_t motion_temp[4][7];//4个小时的7段运动量保存
uint16_t free_fall_temp[4];//4个小时的自由落体次数记录

void Mpu_Samle()
{  
	//不在A0状态下MPU进入休眠状态
	if(app->a0_noraml_state != 1){   
			if(app->mpu_en_flag==1){
				 Write_CTRL_REG1(CTRL_REG1_Config_Sleep);
				 if(Read_CTRL_REG1() == CTRL_REG1_Config_Sleep){		
						app->mpu_en_flag=0;
	  	   }
			}
					
			 motion_hour=0;
			 motion_count=0;
	     current_free_fall_count=0;
			
			 for(uint8_t i=0;i<4;i++) {  
				 for(uint8_t j=0;j<7;j++){//清除4小时7段数据
				    motion_temp[i][j]=0;
				 }
				 free_fall_temp[i]=0;//清除4个小时的自由落体次数记录
			}
			
			//清除总运动量数据
			for(uint8_t i=0;i<All_Motion_Selection_Num;i++){
				all_motion[i]=0;
			}
		  all_motion_idx=0;
			all_motion_count = 0;
			return;
	}
	
	//在A0状态设置为正常模式
	if(app->mpu_en_flag == 0){ 
		Write_CTRL_REG1(CTRL_REG1_Config_Normal);
	  if(Read_CTRL_REG1() == CTRL_REG1_Config_Normal){		
				app->mpu_en_flag=1;
		}
		return;
	}	
	
	//获取各轴加速度
   x=Read_OUT_X();
   y=Read_OUT_Y();
   z=Read_OUT_Z();
	 x1 = Cal_Acc(x);
	 y1 = Cal_Acc(y);
   z1 = Cal_Acc(z);
	
	 /*****计算自由落体*****/
	 if(abs(x1)< Free_Fall_Limit && abs(y1)< Free_Fall_Limit && abs(z1)< Free_Fall_Limit){
	    current_free_fall_count+=1;
		  if(current_free_fall_count>= Free_Fall_Threshold){
			   current_free_fall_count=0;
				 free_fall_temp[motion_hour]+=1;
			}
	 }else{
	     current_free_fall_count=0;
	 }
	 
	  /*****把7段运动量分段存储*****/
	  currentMotion = abs(x1-x0)+ abs(y1-y0) + abs(z1-z0);
	  x0 = x1;y0=y1;z0=z1;   
	  motion_sort(motion_temp[motion_hour],currentMotion);
	 
	 /*****记录次数达到一段时间*****/
	  motion_count +=1;
		if(motion_count>= Motion_Count) 
		{ 
			  motion_hour+=1;
			  app->motion_hour = motion_hour;
			  if(motion_hour>=4){ //累计了4小时
					for(uint8_t i=0;i<4;i++)  //拷贝数据			
					{  
						 for(uint8_t j=0;j<7;j++){
								 app->motion[i][j]= motion_temp[i][j];
								 motion_temp[i][j]=0;
						 }
						 
						 app->free_fall[i]= free_fall_temp[i];
						 free_fall_temp[i]=0;
					}
					motion_hour=0;
				}
				motion_count=0;
		}
		
		/*****计算总运动量*****/
		all_motion[all_motion_idx] += currentMotion;
		all_motion_count +=1;
		//累计时间到达一段
		if(all_motion_count >= All_Motion_Selection_Size){ 
			  all_motion_idx+=1;
			  if(all_motion_idx>=All_Motion_Selection_Num){ //记录时间达到5小时
				   uint32_t sum=0;
					 for(uint8_t i=0;i<All_Motion_Selection_Num;i++){
					    sum+= all_motion[i];
					 }
					 
					 if(sum< All_Motion_Threshold){ //小于阈值需要上报
			        app->all_motion = sum;
				      app->all_motion_need_upload=1;
				   }
					 
					 for(uint8_t i=1;i<All_Motion_Selection_Num;i++){ //数据左移动一位,也是删除最前面5分钟数据
					    all_motion[i-1]=all_motion[i];
					 }
					 all_motion_idx=All_Motion_Selection_Num-1; //下次数据存放在最后的5分钟
					 all_motion[all_motion_idx]=0x00;//清空最后一位数据
				}
   		
			  all_motion_count=0;
		}
}

/*电量测量*/
void Bat_Meausure(void)
{    
	 static uint16_t bat_volt[5]; //保存5次采集的电量
	 static uint8_t sample_count=0;//当前已采样次数
	 static uint32_t last_sample_time =0 ; //最近采样时间
	
	 if((app->base_time - last_sample_time)< Bat_Sample_Interval) //间隔1段时间采样一次
	 {
	     return;
	 }
	 last_sample_time = app->base_time;
	 
	 //采集电量
	 bat_volt[sample_count]= Get_Adc_Average(0,20);
	 sample_count +=1;
	 
	 //采集够了5次才计算一次电量
	 if(sample_count >= Bat_Sample_Count) //采样5次
	 {  
		  uint16_t sum=0;
	    for(uint8_t i=0;i<sample_count;i++)
		 {
		    sum += bat_volt[i];
		 }
		 
		 if(Adc_To_Bq(sum/5) < app->bat_bq && app->bat_bq>1) //电量不允许增加，单次只能减少1
		 {
		    app->bat_bq -=1;
		 }
		 sample_count = 0;
	 }
}

/*开机上报*/
void Sub_Powen_On()
{  
	 //char date[20]; //时间保存缓冲
	 char cmd[32];  //命令装置字节
	 //获取时间
//   while(1)
//	 {
//	    if(Cat1_AT_QLTS(date)==0)
//			{
//			   break;
//			}
//			delay_ms(3000);
//	 }
	 
	 //判断sim卡是电信还是移动
	 int8_t sim_mode=0;
   while(1)
	 {
	    sim_mode = Cat1_AT_QNWINFO();
		  if(sim_mode != -1)
			{
			   break;
			}
			delay_ms(3000);
	 }
	 
	 sprintf(cmd,"PowerOn:00");	
	 if(sim_mode == 1)
	 {
	    cmd[8]='D';
		  cmd[9]='X';
	 }
	 else if(sim_mode == 2)
	 {
	    cmd[8]='Y';
		  cmd[9]='D';
	 }
	 
	 APE_Queue_Pub(cmd);
}


uint8_t A0=0;
uint8_t A1=0;
uint8_t A9=0;
uint8_t A99=0;
uint8_t C0=0;

uint8_t C01=0;
char c01_str[25];
uint8_t c01_idx=0;

uint8_t flag=0;
void Sub_Cmd_Get(uint8_t temp)
{  
	
	 if(flag==0){
		 if(temp=='"'){
		   flag=1;
		 }
	 }else if(flag==1){
	    if(temp=='A'){
			   flag='A';
			}else if(temp=='C'){
			   flag='C';
			}else{
			  flag=0;
			}
	 }else if(flag=='C'){
	    if(temp=='0'){
			   flag='0';
			}else{
			  flag=0;
			}
			
	 }else if(flag=='A'){
		  if(temp=='9'){
				 flag='9';
			}else{
				 if(temp=='0'){
			      A0=1;
				    A1=0;
				}else if(temp=='1'){
						 A1=1;
						 A0=0;
				}
				flag=0;
		}
	 }else if(flag=='9'){
			if(temp=='9'){
					 A99=1;		
				}else if(temp=='"'){
					 A9=1;
				} 
				flag=0;
	 }else if(flag=='0'){
		  if(temp=='"'){
					C0=1;	
			}else if(temp=='1'){
			  flag='1';
				c01_idx=0;
			}else{
			  flag=0;		
			}
	 }else if(flag=='1'){
	   if(temp=='"'){
       C01=1;		    
			 flag=0;
		 }else if(c01_idx<25){
		    c01_str[c01_idx]=temp;
			  c01_idx+=1;
		 }
	 }
}


void Sub_Cmd_Handle()
{    
		 if(A99==1){
				printf("A99 Cmd\r\n");
			  A99=0;
      //  A99_Cmd_Hander("A99");	
        update->upate_flag=0;	 //减少代码尺寸
	      App_Info_Save(); //保存版本号到Flash
      	HAL_NVIC_SystemReset(); //重启进入iap			 
		 }
		 if(A9==1){
				app->a9_reset_state = 1;
				printf("A9 Cmd\r\n");		
        A9=0;			 
		 }
		 if(C0==1){
			 if(app->a1_sleep_state==1){
					printf("C0 Cmd but no action\r\n");
			 }else{
			    app->c0_location_state = 1;
					ape->c0_delay_off_flag=1;
					printf("C0 Cmd\r\n");
			 }		
			  C0=0;
		 }
		 if(C01==1){
			   char lat_str[11],lon_str[10];
			   uint8_t err_flag=0;
			   char *str=strstr(c01_str,":"); //纬度起点
				 if(str==NULL){
						err_flag=1;
					  goto end;
				 }
				 for(uint8_t i=0;i<10;i++){
						lat_str[i]=*(str+1+i);
				 }
				 lat_str[10]='\0';
				 
				 if(*(str+11)!=','){//经度起点
						err_flag=1;
					  goto end;
				 }
				 for(uint8_t i=0;i<9;i++){
						lon_str[i]=*(str+12+i);
				 }
				 lon_str[9]='\0';
				 
				 app->c01_latitued = strtod(lat_str,NULL);
				 app->c01_longitude = strtod(lon_str,NULL);
				 app->c01=1;		
				 end:
				 if(!err_flag){
				     printf("C01 Cmd\r\n");
				 }else{
				   printf("C01 Cmd err\r\n");
				 }
			   C01=0;
		 }
		 if(A0==1){
				app->a1_sleep_state = 0;
				app->a0_noraml_state = 1;
			  app->a0_need_pub_flag = 1;
			  app->c0_location_state = 0;
				ape->c0_delay_off_flag=0;
				printf("A0 cmd\r\n");	
			  A0=0;		 
		 }
		 if(A1==1){
				app->a1_sleep_state = 1;
			  app->a1_need_pub_flag = 1;
				app->a0_noraml_state = 0;
			  app->c01=0;
				printf("A1 cmd\r\n"); 
			  A1=0;
		 }
}

/*A1命令处理*/
void A1_Cmd_hander()
{
	 if(app->a1_need_pub_flag==1)
	 {   
			APE_Queue_Pub("A1");
			app->a1_need_pub_flag=0;
	 }
}

/*A0命令处理*/
void A0_Cmd_Handle(void)
{   
	  char cmd[64];
	  if(app->a1_sleep_state){ 
		   app->a0_need_pub_flag=0;
	  }
	 
    if(app->a0_need_pub_flag == 1){   
				app->bds_need_get_flag=1	;					 
				if(L76k->bds_get_flag==1){
					sprintf(cmd,"A0:%s,%s:%s",L76k->bds,L76k->date,cat1->iccid);	
					app->a0_need_pub_flag=0;		 			  
					APE_Queue_Pub(cmd);
				}if(L76k->bds_get_flag==2){
					app->a0_need_pub_flag=0;		 
          sprintf(cmd,"A0::%s",cat1->iccid);						
					APE_Queue_Pub(cmd);
				}		  
		}			  
}
/*A9命令处理*/
void A9_Cmd_Hander()
{  
   if(app->a9_reset_state == 1){
			APE_Queue_Pub("A9");
			app->a9_reset_state=0;
		  Cat1_Power_Off();//cat1关机
	 }
}

/*C0命令处理*/
void C0_Cmd_Hander()
{
   static uint8_t have_send_flag=0;//已经上传了一次
	 static uint32_t send_success_time=0; //发送成功的时刻
	 char cmd[64];
	
	 if(app->a1_sleep_state){ 
		  app->c0_location_state=0;
	 }
	 
	//已经发送后10分钟收到不会再发送
	 if(have_send_flag==1){
	    if((app->base_time - send_success_time)>C0_Keep_Time){
			   have_send_flag=0;
			}else{
			   app->c0_location_state=0;
			}
	 }
	 
	 if(app->c0_location_state==1){
	     app->bds_need_get_flag=0x01;
			 if(L76k->bds_get_flag==1){// //获取成功bds和时间就上报C0:Bds:Date  
           sprintf(cmd,"C0:%s,%s",L76k->bds,L76k->date);					 
					 APE_Queue_Pub(cmd);
				   send_success_time=app->base_time;
				   have_send_flag=1;
			 }else if(L76k->bds_get_flag==2){// //获取不到上报C0      			
					 APE_Queue_Pub("C0");
				   send_success_time=app->base_time;
				   have_send_flag=1;
			 }
	 }
}

/*C1命令处理*/
void C1_Hander()
{  
	 static uint8_t send_c1_flag=0;//已经上传了c1
   static uint8_t send_c1_bds_date_flag=0;//已经上传了c1_bds_date
	 static uint32_t send_success_time=0; //发送成功的时刻
   char cmd[64];
	
	 if(app->a1_sleep_state==1){ 
		  app->m_sw_state=0;
		  ape->c0_delay_off_flag=0;
		  send_c1_flag=0;
	 }
	//已经发送后10分钟收到不会再发送
	 if(send_c1_bds_date_flag==1){
	    if((app->base_time - send_success_time)>C1_Keep_Time){
			   send_c1_bds_date_flag=0;
			}else{
			   app->m_sw_state=0;
			}
	 }
	 
	 if(app->m_sw_state==1){  
			 //上报C1
				if(send_c1_flag==0){			
		      sprintf(cmd,"C1::%d:%d",m_sw->sw, m_sw->sw1);				
					APE_Queue_Pub(cmd);
					send_c1_flag  = 1;
				}
				//上报C1:Bds
			 if(send_c1_flag==1){   
					 app->bds_need_get_flag=1;
				   if(L76k->bds_get_flag==2){ //获取gps失败
//						  sprintf(cmd,"CC1::%d:%d",m_sw->sw, m_sw->sw1);				
//					    APE_Queue_Pub(cmd);
					    send_c1_bds_date_flag=1;
						  send_c1_flag=0;
						  send_success_time = app->base_time;
					 }else if(L76k->bds_get_flag==1){
						  sprintf(cmd,"C1:%s,%s:%d:%d",L76k->bds,L76k->date,m_sw->sw, m_sw->sw1);
						  APE_Queue_Pub(cmd);
						  send_c1_bds_date_flag  = 1;
						  send_c1_flag=0;
						  send_success_time = app->base_time;
					 }
			 }
	 }	
}

void C2_Hander()
{
   static uint8_t c2_motion_flag=0;//c2_motion_date发送标志
	 static uint8_t have_send_flag=0; //已经发送标志
	 static uint32_t send_success_time=0;
   char cmd[64];
	
	 if(app->a1_sleep_state==1){
	   app->all_motion_need_upload=0;
		 c2_motion_flag=0;
		
	 }
	 if(have_send_flag==1){
	    if((app->base_time -send_success_time)>C2_Keep_Time){
			   have_send_flag=0;
			}else{
			   app->all_motion_need_upload=0;
			}
	 }
	 if(app->all_motion_need_upload==1)
	 {  
		  //上报C2:Motion
			if(c2_motion_flag==0){
				 sprintf(cmd,"C2::%d",app->all_motion);	
				 APE_Queue_Pub(cmd);
				 c2_motion_flag = 1;
			}
			
		 //上报C2:Bds
		 if(c2_motion_flag==1){   
				 app->bds_need_get_flag=1;
				 if(L76k->bds_get_flag==2){ //获取gps失败
						 app->all_motion_need_upload=0;
						 c2_motion_flag=0;
					   have_send_flag=1;
					 send_success_time=app->base_time;
				 }else if(L76k->bds_get_flag==1){//获取bds成功
						sprintf(cmd,"C2:%s,%s:%d",L76k->bds,L76k->date,app->all_motion);	
						APE_Queue_Pub(cmd);  
						app->all_motion_need_upload=0;
						c2_motion_flag=0;	
            have_send_flag=1;			
            send_success_time=app->base_time;					 
				 }
			}
	 }	
}


void C5_C7_Hander()
{
	 if(app->a1_sleep_state==1){
		 app->motion_hour=0;
	}
	
	 if(app->motion_hour==4){  
		  char cmd[192]={0};
			//发送C5
		  sprintf(cmd, "C5:%d,%d,%d,%d,%d,%d,%d:%d,%d,%d,%d,%d,%d,%d:%d,%d,%d,%d,%d,%d,%d:%d,%d,%d,%d,%d,%d,%d",
			app->motion[3][0],app->motion[3][1],app->motion[3][2],app->motion[3][3],app->motion[3][4],app->motion[3][5],app->motion[3][6],
			app->motion[2][0],app->motion[2][1],app->motion[2][2],app->motion[2][3],app->motion[2][4],app->motion[2][5],app->motion[2][6],
			app->motion[1][0],app->motion[1][1],app->motion[1][2],app->motion[1][3],app->motion[1][4],app->motion[1][5],app->motion[1][6],
			app->motion[0][0],app->motion[0][1],app->motion[0][2],app->motion[0][3],app->motion[0][4],app->motion[0][5],app->motion[0][6]);
			APE_Queue_Pub(cmd);
			
			//发送C7
			sprintf(cmd, "C7:%d:%d:%d:%d",app->free_fall[3],app->free_fall[2],app->free_fall[1],app->free_fall[0]);
			APE_Queue_Pub(cmd);

		  app->motion_hour=0;
	 }
}

/*C6电量和CSQ上报*/
void C6_Hander(void)
{    
	 static uint32_t last_upload_time=0;//上次上传时刻
	 uint8_t csq=0;
	 char cmd[32];
	
	 if((app->base_time - last_upload_time) > Bat_UP_Interval ) { 
		   app->need_pub_c6 = 1;
	 }
	  
	 if(app->need_pub_c6==1){   
			 if(Cat1_AT_CSQ(&csq)==0){ //获取信号强度
				  sprintf(cmd,"C6:%d:%d",app->bat_bq,csq);	
					APE_Queue_Pub(cmd);
					app->need_pub_c6=0;
				  last_upload_time = app->base_time;
			 }
   }
}

uint16_t sec;
void Ape_Hander()
{   
	 char date[20]; //时间存放缓冲
	 static uint32_t  heart_interval= A0_Day_Heart_Time;
	 static uint32_t previous_heart_time=0;
	 if(app->a0_noraml_state==0) { //A0模式才处理
	    return;
	 }	
    if(ape->conect_flag==1){
				if((ape->c0_delay_off_flag==0 && (app->base_time- ape->last_send_time)>2*Second)   //等待平台是否有数据的时间，太短会出现关机次数多点
					||(ape->c0_delay_off_flag==1 && (app->base_time- ape->last_send_time)>1800*Second)){ //收到C0 30分钟再关机 30分钟
						 
						//根据时间段确认开机时间
						 if(Cat1_AT_QLTS(date)==0){
								 sec=((date[8]-0x30)*10+date[9]-0x30)*3600 +((date[10]-0x30)*10+date[11]-0x30)*60;
								 if(sec>=27000 && sec<63000){ //7时30分00秒-17时29分59秒 
										heart_interval=A0_Day_Heart_Time;
								 }else{
										heart_interval=A0_Night_Heart_Time;
								 }
						 }
						 //关机
						 Cat1_Power_Off();
						 ape->c0_delay_off_flag=0;
				}
		}else{
		   if((app->base_time- previous_heart_time)>heart_interval && (app->base_time- ape->last_send_time)>heart_interval){ 
				  Cat1_Power_On();
				  previous_heart_time =app->base_time;
			  // Ape_Send("heart_beat");
			 }
		}
}


void Low_Power_Hander()
{  
	 static uint32_t exit_sleep_time=0; //退出休眠时间
	 static uint8_t exit_sleep_flag=0; //退出休眠标志
	 static uint32_t enter_sleep_time=0; //进入休眠时间
	
	 if(exit_sleep_flag==1){
		  if((app->base_time - exit_sleep_time)>10*Second){
					 exit_sleep_flag=0;
		  }
	    return;
	 }
	
	 if(app->a1_sleep_state==0) {
	    return;
	 }		 
	
	 if(app->mpu_en_flag !=0){
	    return;
	 }
	 

	 Cat1_Power_Off();
	 printf("enter sleep\r\n");
	 delay_ms(20);
	 LPUART1_DeInit();//关闭串口1;
	 HAL_TIM_Base_Stop_IT(&TIM2_Handler);//关闭定时器2		
	 LPTIM1_Start(); // 启动LPTIM1唤醒
	 enter_sleep_time= app->base_time;
	 
	 //休眠等待串口唤醒，或者休眠时间到
	 while(1){
	    HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);	
		  if((app->base_time - enter_sleep_time)>=A1_Heart_Time){
			    break;
			}
	 }
    
	 //退出休眠
	 SystemClock_Config();	 
	 LPUART1_Init(9600);
	 HAL_TIM_Base_Start_IT(&TIM2_Handler); //使能定时器2
	 LPTIM1_Stop(); // 关闭LPTIM1唤醒
	 printf("wake up\r\n");	
	 
	 delay_ms(200);
	 if(ape->conect_flag==0){  //如果平台没连接过，就连接一次
	  // Ape_Send("heart_beat");
		  Cat1_Power_On();
	 }
   	 
	 exit_sleep_time = app->base_time;
	 exit_sleep_flag = 1;
}

