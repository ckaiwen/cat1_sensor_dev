#ifndef __APP_H
#define __APP_H
#include "stm32l0xx.h"
#include "stdlib.h"
#include "delay.h"

#include "usart.h"
#include "TIM1.h"
#include "TIM.h"

#include "Bat.h"
#include "M_SW.h"
#include "lis3dh_SPI.h"
#include "EC800.h"
#include "L76K.h"
#include "APE_MQTT.h"
#include "Update.h"

#define Debug_En           0   //0为关闭串口调试，1为打开
#define Second             20  //base_time单位为50ms，所以1s等于20

#define A1_Heart_Time        3600*Second  //A1心跳时间
#define A0_Day_Heart_Time    1800*Second  //A0白天心跳时间
#define A0_Night_Heart_Time  3600*Second  //A0晚上心跳时间

#define Cat1_Max_Err_Count   5  //AT命令发给Cat1，返回错误或者不返回次数超过等于该次数，Cat1模块重启一次

#define Bat_Sample_Interval    600*Second //电池电量采集间隔时间
#define Bat_Sample_Count       5 //电池电量采集次数
#define Bat_UP_Interval        86400*Second //电池电量上报时间

//GPS
#define GPS_Keep_Time          600*Second //获取GPS后，这段时间不会再获取，为了省电
#define Get_BDS_Time           120*Second //A0状态下获取GPS时间

#define C0_Keep_Time            300*Second //C0发送成功后在该时间内收到C0不会再发送
#define C1_Keep_Time            300*Second //C1发送成功后在该时间内收到C1不会再发送

#define C2_Keep_Time           1800*Second //C2发送成功后该时间段内不会再发送

//#define All_Motion_Threshold       1526100  //总运动量阈值，小于该值需要上报
#define All_Motion_Threshold         685000  //总运动量阈值，小于该值需要上报，半个钟比以前少一半
#define All_Motion_Selection_Num    6 //总运动量段数,6段
#define All_Motion_Selection_Size   300*Second //总运动量每一段大小，5分钟

#define Motion_Count            3600*Second  //运动量4段中每段时间
#define Free_Fall_Threshold     4   //连接多少次检测到自由落体相当是自由落体
#define Free_Fall_Limit         100   //都小于100mg为失重

//电子围栏
//#define Fence_Distance      5.0   //电子围栏范围(1.5km)
#define Fench_Latitued        0.013514 //纬度   0.01666667度为1分  按1分1.85km计算
#define Fench_Longitude       0.013514  //经度
typedef struct
{  
	 uint32_t base_time; //基准时间,单位为50ms
	 
	 uint8_t c01;
	 double  c01_latitued; //纬度
	 double  c01_longitude; //经度
	
	 uint8_t c1_need_bds_flag;
	 uint8_t c2_need_bds_flag;
	 
	 uint8_t bds_need_get_flag;
	
	 uint8_t need_pub_c6 ;//需要发C6：电量:csq标志
	
   uint8_t a1_sleep_state; //a1命令状态
	 uint8_t a1_need_pub_flag; //a1需要上报标志
	
	 uint8_t a0_noraml_state;//a0命令状态
	 uint8_t a0_need_pub_flag;//a0需要上报标志
	 
	 uint8_t a9_reset_state; //a9重启命令
	 
	 uint8_t c0_location_state;//c0命令状态
	 
	 uint8_t m_sw_state;  //磁铁感应状态
	 
	 uint8_t mpu_en_flag; //mpu使能状态
	 
	 uint8_t all_motion_need_upload;//总运动量需要上报标志
	 uint32_t all_motion;//总运动量
	 
	 uint8_t  motion_hour; //mpu4小时中哪个小时
	 uint32_t motion[4][7];//mpu4段7个等级运动量
	 uint16_t free_fall[4];//自由落体4小时次数
	
   uint8_t bat_bq;//电池电量百分比	
	 
	 uint32_t last_heart_time;
}APP_Struct;


extern APP_Struct *app;

void App_Start(void);
void Mpu_Samle(void);//mpu数据采集
void Bat_Meausure(void);
void Sub_Powen_On(void);
void Sub_Cmd_Get(uint8_t temp);
void Sub_Cmd_Handle(void);
void BDS_Dev_Handle(void);
void A0_Cmd_Handle(void);
void A1_Cmd_hander(void);
void A9_Cmd_Hander(void);
void C0_Cmd_Hander(void);
void C1_Hander(void);
void C2_Hander(void);
void C5_C7_Hander(void);
void C6_Hander(void);//C6电量和CSQ上报
void Ape_Hander(void);

void Low_Power_Hander(void);
#endif


