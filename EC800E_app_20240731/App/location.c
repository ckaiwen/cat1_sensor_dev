#include "Location.h"

Loc_Struct loc_struct;
Loc_Struct *loc = &loc_struct;


int8_t Loc_Get_Token()
{
   char* str= Cat1_Send("AT+QLBSCFG=\"token\"\r\n",20,"OK",1000);
	 if(str == NULL){
	     return -1;
	 }
	 return 0;
}	 

int8_t Loc_AT_Set_Token()
{  
   if(Cat1_Send("AT+QLBSCFG=\"token\",\"69qLt3Tl40BdU597\"\r\n",0x32,"OK",1000)==NULL){
	     return -1;
	 }
	 return 0;
}	 


int8_t Loc_Set_Data_Foramt()
{  
	 char cmd[32];
	 uint16_t len=sprintf(cmd,"AT+QLBSCFG=\"latorder\",1\r\n");
	
   char* str= Cat1_Send(cmd,len,"OK",1000);
	 if(str == NULL){
	     return -1;
	 }
	 return 0;
}	 


int8_t Loc_Get_Data()
{
   char* str= Cat1_Send("AT+QLBS=0\r\n",11,"OK",3000);
	 if(str == NULL){
	     return -1;
	 }
	 
	 char lat_str[11],lon_str[10];
	 
	 str=strstr(str,","); //经度起点
	 if(str==NULL){
	    return -1;
	 }
	 for(uint8_t i=0;i<10;i++){
	    lat_str[i]=*(str+1+i);
	 }
	 lat_str[10]='\0';
	 
	 if(*(str+11)!=','){
	    return -1;
	 }
	 for(uint8_t i=0;i<9;i++){
	    lon_str[i]=*(str+12+i);
	 }
	 lon_str[9]='\0';
	 
	 loc->latitued = strtod(lat_str,NULL);
	 loc->longitude = strtod(lon_str,NULL);
	 return 0;
}

void Power_On_Loc_Hander()
{  
	if(loc->need_get_loc==0){
	   return;
	}
	loc->need_get_loc=0;
	
	 char cmd[32];
	 uint8_t loc_get_flag=0;
	 int8_t count=3;
	 while(count>0){
		 if(Loc_Get_Data()==0){ //获取gps
				sprintf(cmd,"C02:%f,%f",loc->latitued,loc->longitude);	
				APE_Queue_Pub(cmd);  //上报C02
			  loc_get_flag=1;
			  break;
		 }else{
		   count-=1;
			 delay_ms(3000);
		 }
   }
	 
	 if(app->c01==0x00 && app->a0_noraml_state==1){  //没获取到C01且在A0模式下就上报C01:null
		  APE_Queue_Pub("C01:null");
	 }
	
	 if(app->c01==0x00 || loc_get_flag==0x00){ //gps数据缺失，直接返回
	    return ;
	 }
	 if( (loc->latitued - app->c01_latitued)> Fench_Latitued || (app->c01_latitued - loc->latitued )>Fench_Latitued
		 ||(loc->longitude - app->c01_longitude)> Fench_Longitude || (app->c01_longitude - loc->longitude)>Fench_Longitude){
		    app->c0_location_state=0x01; //开启C01命令
			  ape->c0_delay_off_flag=1; //C01命令需要休眠一段时间再关机命令
		 }
}

