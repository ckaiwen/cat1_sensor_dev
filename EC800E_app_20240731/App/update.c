#include "Update.h"

Update_Struct  update_struct;
Update_Struct  *update = &update_struct;

 uint8_t buf[128];
void Update_Init()
{  
	 char date[20]={0};
	 char cmd[64];
	 
	 Flash_Page_Read(Data_Page,buf);
	 if(buf[0] != Need_Check_Update) //不需要确认更新信息直接返回
	 {
	    return ;
	 }
	 
	 //赋值更新成功并保存
	 buf[0]=Update_Success_Flag;
	 Flash_Write(Data_Page,buf,32);
	 
	 //拷贝版本号
    uint8_t i=0;
	  for(i=0;i<(Version_Len-1);i++)
		{  
		   update->update_version[i]=buf[1+i];
		}
		update->update_version[i] = '\0';
	
	//获取时间
	 uint8_t err=5;
   while(err>=1)
	 {
	    if(Cat1_AT_QLTS(date)==0){
			   break;
			}
			err -=1;
			delay_ms(3000);
	 }
	 
	 sprintf(cmd,"A99:%s:%s",date,update->update_version);	
	 APE_Queue_Pub(cmd);
}


void A99_Cmd_Hander(char *recv_str)
{ 
	uint8_t i=0;
	char *str = strstr(recv_str,"A99");//A99:20240108"
	str = strstr(str,":"); //:20240108"
	if(str ==NULL) //没有版本号，默认更新最新的
	{
	   update->upate_flag = 0x00;
	}
	else //有版本号获取版本号
	{
	   for(i=0;i<(Version_Len-1);i++)
		{  
			 if(*(str+1+i)=='"')
			 {
			    break;
			 }
		   update->update_version[i]=*(str+1+i);
		}
		update->update_version[i] = '\0';
	}
	
	App_Info_Save(); //保存版本号到Flash
	HAL_NVIC_SystemReset(); //重启进入iap
}



int8_t App_Info_Save()
{
	 buf[0]= update->upate_flag; //更新方式
	 for(uint8_t i=0;i<20;i++)
	 {
	    buf[1+i]= update->update_version[i];
	 }
	 return Flash_Write(Data_Page,buf,32);
}
