#ifndef __APE_MQTT_H
#define __APE_MQTT_H
#include "stm32l0xx.h"
#include "EC800.h"
#include "App.h"

/****对接ape平台相关****/
//cfk_test平台
//#define APE_Product_Id  "17004186"  //ape平台上mqtt产品的id
//#define APE_Product_Feature "gTlkJDygMyvriK1RX2dRqtZALLSQ8SmdEBN6lt_6V4Q" ////ape平台上mqtt产品的特征串(一型一密)
//#define APE_Tcp "2000131622.non-nb.ctwing.cn"
//王总ctwing平台
#define APE_Product_Id  "17040095"  //ape平台上mqtt产品的id
#define APE_Product_Feature "y1f4DkR6o03rvFlZshCXMkg9EGmctZY82X_v_tupqNE" ////ape平台上mqtt产品的特征串(一型一密)
#define APE_Tcp "2000093297.non-nb.ctwing.cn"

#define APE_User_Name "cfk_test" //用户名称,可以用ape平台的账号作为名称
//APE_Dev_No 设备编号：以每台设备以设备序列号IMEI作为设备的编号
#define APE_Sub_Topic "device_control"  //订阅主题
#define APE_Sub_Qos  2 //0最多发送一次;1至少发送一次;2只发送一次
#define APE_Pub_Topic "device_update"  //上报主题
#define APE_Pub_Qos  2 //0最多发送一次;1至少发送一次;2只发送一次

#define Pub_RetryNum          3           //数据上报尝试次数
#define Pub_Err_Interval      3600*Second //数据上传出现错误后，间隔多久才能再上传一次


/*发送数据队列*/
#define Pub_Queue_Num   25  //需要上传的数据队列大小
#define Pub_Queue_Size  188  //需要上传的数据队列大小


typedef struct
{  
	 uint32_t send_count;
	 uint8_t conect_flag;
	 uint8_t c0_delay_off_flag;
	 uint32_t last_send_time; //最后数据上报时间
	 char pub_queue[Pub_Queue_Num][Pub_Queue_Size]; //队列缓冲区
}APE_Struct;
 

extern APE_Struct  *ape;

int8_t Ape_Send(char *content); //直接发送
int8_t Mqtt_QMTCLOSE(void); //关闭mqtt
int8_t APE_Queue_Pub(char *content); //以队列方式发送数据到平台
void APE_Pub_Queue_handler(void); //ape队列发射处理
int8_t Mqtt_QMTDISC(void);//客户端断开与MQTT服务器的连接
#endif


