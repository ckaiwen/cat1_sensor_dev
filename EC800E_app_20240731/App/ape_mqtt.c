#include "APE_MQTT.h"

APE_Struct  ape_struct;
APE_Struct  *ape = &ape_struct;

int8_t Mqtt_Cfg_KeepAliveTime()
{
	 if(Cat1_Send("AT+QMTCFG=\"keepalive\",0,300\r\n",29,"OK",3000) == NULL){
	     return -1;
	 }
	 return 0;
}
//打开MQTT客户端网络
int8_t Mqtt_QMTOPEN()
{  
	 char cmd[64]={0};
   uint16_t len=sprintf(cmd,"AT+QMTOPEN=0,\"%s\",1883\r\n", APE_Tcp);
   char* str= Cat1_Send(cmd,len,"QMTOPEN",3500);
	 if(str == NULL){
	     return -1;
	 }
	 
	 str = strstr(str,"0,");
	 if(str == NULL){
	     return -1;
	 }
	 return *(str+2)-0x30;
}
//关闭MQTT客户端网络
int8_t Mqtt_QMTCLOSE()
{  
    char* str= Cat1_Send("AT+QMTCLOSE=0\r\n",15,"QMTCLOSE",2000);
	  if(str == NULL || strstr(str,"OK")==NULL){
	     return -1;
	 }
	 return 0;
}


//连接客户端到MQTT服务器
int8_t Mqtt_QMTCONN(char *pruduct_id,char *dev_no,char *user_name,char *password)
{  
	 char cmd[128]={0};
	 uint16_t len= sprintf(cmd,"AT+QMTCONN=%d,\"%s%s\",\"%s\",\"%s\"\r\n",0, pruduct_id,dev_no,user_name,password);	
   char* str= Cat1_Send(cmd,len,"QMTCONN",3000);
	 if(str == NULL || strstr(str,",0")==NULL){
	     return -1;
	 }
	 return 0;
}

//客户端断开与MQTT服务器的连接
int8_t Mqtt_QMTDISC()
{  
   char* str= Cat1_Send("AT+QMTDISC=0\r\n",14,"QMTDISC",3000);
	 if(str == NULL || strstr(str,"OK")==NULL){
	     return -1;
	 }
	 return 0;
}


int8_t APE_Mqtt_Connect()
{  
	 uint8_t state=0;
	 uint8_t pdp_err_count=0;
	
//	 Mqtt_QMTCLOSE();
	 Cat1_Send("AT+QMTCLOSE=0\r\n",15,"QMTCLOSE",2000); //减少代码大小
	 delay_ms(100);
	// Mqtt_Cfg_KeepAliveTime();//设置keep alive时间
	 Cat1_Send("AT+QMTCFG=\"keepalive\",0,270\r\n",29,"OK",3000); //设置cat1休眠模式心跳keep alive时间
	 
	 while(1){
	    state=Mqtt_QMTOPEN();
		  if(state==3){ //激活 PDP 失败
				 pdp_err_count+=1;
				 if(pdp_err_count>=3){
				   return -1;
				 }
			   delay_ms(1000);
			}else if (state==0){
			   break;
			}else{
			    return -1;
			}
	 }	
	
	 delay_ms(100);
	 //连接客户端到MQTT服务器
	 if(Mqtt_QMTCONN(APE_Product_Id,cat1->imei,APE_User_Name,APE_Product_Feature)!=0){
			return -1;
	 }
	 
	 return 0;
	 #undef Retry_Time
}


//发布主题
int8_t Mqtt_QMTPUBEX(char *topic,uint8_t qos,char *content)
{
   char cmd[256]={0};
	 static uint16_t msg_id=0;
	 uint16_t have_rec_time=0; 
//	 if(qos==0)//只有当qos=0 时，该参数值为 0
//	 {
//	    msg_id =0; 
//	 }
//	 else
//	 {
//	    msg_id+=1;
//	 }
	 msg_id+=1;
	 //发送，等待返回"<"
	 uint16_t len= sprintf(cmd,"AT+QMTPUBEX=%d,%d,%d,0,\"%s\",%d\r\n",0, msg_id,qos,topic,strlen(content));	
   Cat1_Send(cmd,len,">",200);
	 while(have_rec_time<2000){
		  if(com2->rx_len>=1 && strstr((char*)com2->rx_buf,">")){
	      break;
	    }
	    delay_ms(1);
			have_rec_time+=1;
	 }
//	 if(have_rec_time>=2000){
//	    return -1;
//	 }
	 delay_ms(50);
	 
	 char *str= Cat1_Send(content,strlen(content),"+QMTPUBEX",3000);
	 if(str == NULL || strstr(str,",0")==NULL ){
	     return -1;
	 }
	 return 0;
}

int8_t Ape_Send(char *content)
{  
	 uint8_t err_count=0;
	 while(err_count<Pub_RetryNum){  //每条数据上报都尝试几次
		 if(ape->conect_flag==0){  //未连接就连接
				if(APE_Mqtt_Connect()==0){
					 ape->conect_flag=1;
				}else{
					 err_count+=1;
					// printf("ape conenct err:%d\r\n",err_count);
					 continue;
				}
		 }
		 
		  if(Mqtt_QMTPUBEX(APE_Pub_Topic,APE_Pub_Qos,content)!=0){ //发送失败相当断开处理
				 ape->conect_flag=0;
				 err_count+=1;
				// printf("ape pub err:%d\r\n",err_count);
		 }else{
		    break;
		 }
   }
	 ape->last_send_time = app->base_time;
	 if(err_count>=Pub_RetryNum){
	    return -1;
	 }else{
	   return 0;
	 }
}

//以队列方式上报数据到Ape平台
//返回-1数据长度过长,正确返回0
uint8_t send_start_idx=0;
int8_t APE_Queue_Pub(char *content)
{   
	  static uint8_t idx =0;
	  //判断数据长度
	  uint8_t len = strlen(content);
	  if(len >= (Pub_Queue_Size-8)){ //需要上报命令序号，预留6个字节
		   return -1;
		}
		
		//队列索引超过最大值，又从第一个开始
    if(idx>=Pub_Queue_Num){
		   idx=0;
		}
		
	  //拷贝数据
		if(ape->pub_queue[idx][0]!=0x00){ //第0个字节不为0，说明队列满，发生丢弃在情况,下次发送需要从idx+1开始
		   send_start_idx =idx+1;
		}
		sprintf(&ape->pub_queue[idx][1],"%d:%s",ape->send_count,content);
		ape->send_count+=1;
		//每个队列的第一个数据为01代表接受到数据
		ape->pub_queue[idx][0]=0x01;
		idx +=1;
		
		return 0;
}

//ape队列发射处理
void APE_Pub_Queue_handler()
{
	 static uint32_t  wait_start_time=0;
	 static uint32_t need_wait_time=0;
	
	//等待间隔时间到后才能发送
	 if((app->base_time - wait_start_time)<=need_wait_time){ 
		  Cat1_Power_Off();
			return;
	 }
	 	
	 for(uint8_t i=0;i<Pub_Queue_Num;i++){
	    //队列索引超过最大值，又从第一个开始		 
			if(send_start_idx>=Pub_Queue_Num){
				 send_start_idx=0;
			}
			if(ape->pub_queue[send_start_idx][0]==0) {  
				 send_start_idx+=1;
			   continue;
			}
			//发送成功把需要发送标志置0
			if(Ape_Send(&ape->pub_queue[send_start_idx][1]) == 0) {  //发送正确
				 ape->pub_queue[send_start_idx][0]=0x00;
				 send_start_idx+=1;
			}else{  //发送错误
				 need_wait_time = Pub_Err_Interval;
				 wait_start_time = app->base_time;
				 return;
			}
	 }
}

