#include "TIM1.h"


LPTIM_HandleTypeDef hlptim1;
uint16_t tim1_psc;

void LPTIM1_Init(u16 arr,u16 psc)
{  
  hlptim1.Instance = LPTIM1;
  hlptim1.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;  //选择内部时钟源
  hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV128;           //设置LPTIM时钟分频
  hlptim1.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;                        //设置软件触发
  hlptim1.Init.Trigger.ActiveEdge = LPTIM_ACTIVEEDGE_RISING;                      //设置上升沿触发
  hlptim1.Init.Trigger.SampleTime = LPTIM_TRIGSAMPLETIME_DIRECTTRANSITION;        //设置时钟干扰滤波器
  hlptim1.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;     //设置输出高电平
  hlptim1.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;            //比较寄存器和ARR自动重载寄存器选择更改后立即更新
  hlptim1.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;   //LPTIM计数器对内部时钟源计数
  HAL_LPTIM_Init(&hlptim1) ;
	
	tim1_psc=psc;
}


void LPTIM1_Start()
{ 
	
   HAL_LPTIM_TimeOut_Start_IT(&hlptim1, 0, tim1_psc);
}

void LPTIM1_Stop()
{
   HAL_LPTIM_TimeOut_Stop_IT(&hlptim1);
}


void HAL_LPTIM_MspInit(LPTIM_HandleTypeDef* hlptim)
{
  if(hlptim->Instance==LPTIM1)
  {
    __HAL_RCC_LPTIM1_CLK_ENABLE();
		 HAL_NVIC_SetPriority(LPTIM1_IRQn, 1, 3);
    HAL_NVIC_EnableIRQ(LPTIM1_IRQn);
  }
}


void LPTIM1_IRQHandler(void)
{
    HAL_LPTIM_IRQHandler(&hlptim1);
}


void HAL_LPTIM_CompareMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
//   app->base_time += tim1_psc*128*Second/37000;
	 app->base_time+=4534;
}

