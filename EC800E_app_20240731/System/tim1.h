#ifndef __TIM1_H
#define __TIM1_H

#include "stm32l0xx_hal.h"
#include "sys.h"
#include "app.h"

extern LPTIM_HandleTypeDef hlptim1;

void LPTIM1_Init(u16 arr,u16 psc);
void LPTIM1_Start(void);
void LPTIM1_Stop(void);
#endif


