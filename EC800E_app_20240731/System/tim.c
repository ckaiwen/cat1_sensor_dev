#include "TIM.h"


TIM_HandleTypeDef TIM2_Handler;      //定时器句柄 

//通用定时器2中断初始化
//arr：自动重装值。
//psc：时钟预分频数
//定时器溢出时间计算方法:Tout=Ft/((arr+1)*(psc+1)) us.
//Ft=定时器工作频率,单位:Mhz
//这里使用的是定时器2!
void TIM2_Init(u16 arr,u16 psc)
{  
    TIM2_Handler.Instance=TIM2;                          //通用定时器2
    TIM2_Handler.Init.Prescaler=psc;                     //分频系数
    TIM2_Handler.Init.CounterMode=TIM_COUNTERMODE_UP;    //向上计数器
    TIM2_Handler.Init.Period=arr;                        //自动装载值
    TIM2_Handler.Init.ClockDivision=TIM_CLOCKDIVISION_DIV1;//时钟分频因子
    HAL_TIM_Base_Init(&TIM2_Handler);
    
    HAL_TIM_Base_Start_IT(&TIM2_Handler); //使能定时器2和定时器3更新中断：TIM_IT_UPDATE   
}

//定时器底册驱动，开启时钟，设置中断优先级
//此函数会被HAL_TIM_Base_Init()函数调用
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
{
    if(htim->Instance==TIM2)
	{
		__HAL_RCC_TIM2_CLK_ENABLE();            //使能TIM2时钟
		HAL_NVIC_SetPriority(TIM2_IRQn,1,3);    //设置中断优先级，抢占优先级1，子优先级3
		HAL_NVIC_EnableIRQ(TIM2_IRQn);          //开启ITM3中断   
	}
}

//定时器2中断服务函数
void TIM2_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&TIM2_Handler);
}

//回调函数，定时器中断服务函数调用,50ms
#define Sw_Trigger_Time 60 //50ms一次,60次等于3S
uint16_t sw_change_count=0;
//uint8_t state=0;
//uint8_t state1=0;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if(htim==(&TIM2_Handler))
    {
       app->base_time +=1;
//			 state=M_Sw_State_Get();  //u5
//			 state1=M_Sw_State_Get1();  //u4
			 if(m_sw->sw1 !=M_Sw_State_Get1()){
				 sw_change_count+=1;
			   if(sw_change_count >= Sw_Trigger_Time) { //状态变化超过一定次数才相对状态改变
				    app->m_sw_state = 1;
				    m_sw->sw =M_Sw_State_Get();    //u5
 				    m_sw->sw1 =M_Sw_State_Get1();  //u4
					  sw_change_count=0;
				 }
			 }else{
			    sw_change_count=0;
			 }
			
			
			 Mpu_Samle();
    }
}

//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
//{
//    if(htim==(&TIM2_Handler))
//    {
//       app->base_time +=1;
//				
//			 if(m_sw->sw !=M_Sw_State_Get() ||  m_sw->sw1 !=M_Sw_State_Get1()){
//				 sw_change_count+=1;
//			   if(sw_change_count >= Sw_Trigger_Time) { //状态变化超过一定次数才相对状态改变
//				    app->m_sw_state = 1;
//				    m_sw->sw =M_Sw_State_Get();
// 				    m_sw->sw1 =M_Sw_State_Get1();
//					  sw_change_count=0;
//				 }
//			 }else{
//			    sw_change_count=0;
//			 }
//			
//			
//			 Mpu_Samle();
//    }
//}

