#ifndef __STM_Flash_H
#define __STM_Flash_H

#include "stm32l0xx_hal.h"
#include "sys.h"

void Flash_Page_Read(uint8_t page,uint8_t *data);
//int8_t  Flash_Page_Write(uint8_t page,uint8_t *data);
int8_t  Flash_Write(uint8_t page,uint8_t *data,uint32_t len);
#endif


