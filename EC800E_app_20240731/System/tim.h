#ifndef __TIM_H
#define __TIM_H

#include "stm32l0xx_hal.h"
#include "sys.h"
#include "usart.h"
#include "App.h"

extern TIM_HandleTypeDef TIM2_Handler;
void TIM2_Init(u16 arr,u16 psc);
#endif


