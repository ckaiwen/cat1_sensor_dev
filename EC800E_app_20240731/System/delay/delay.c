#include "delay.h"


uint8_t fac_us=0;			//us延时倍乘数	

/*-----------------------------------------
 * 函数名   : delay_init
 * 输入参数 ：sysclk :系统时钟频率   
 * 输出参数 ：无
 * 返回值   ：无
 * 函数功能 ：SysTick定时器初始化
-----------------------------------------*/
void delay_init(uint8_t sysclk)
{
	uint32_t reload;
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);//选择外部时钟  HCLK
	fac_us=sysclk;				//fac_us需要使用
	reload=sysclk;				//每秒钟的计数次数 单位为M  
	reload*=1000000/1000;						//设定溢出时间1ms

	SysTick->CTRL|=SysTick_CTRL_TICKINT_Msk;   	//开启SYSTICK中断
	SysTick->LOAD=reload; 						//每1ms中断一次	
	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk;   	//开启SYSTICK    
	HAL_NVIC_EnableIRQ(SysTick_IRQn);        //使能SysTick中断通道
  HAL_NVIC_SetPriority(SysTick_IRQn,3,3);  //抢占优先级3，子优先级3
}

/*-----------------------------------------
 * 函数名   : delay_us
 * 输入参数 ：nus:要延时的us数.0~204522252(最大值即2^32/fac_us@fac_us=168)
 * 输出参数 ：无
 * 返回值   ：无
 * 函数功能 ：延时n微秒
-----------------------------------------*/
//void delay_us(uint32_t nus)
//{	
//	uint32_t ticks;
//	uint32_t told,tnow,tcnt=0;
//	uint32_t reload=SysTick->LOAD;			//LOAD的值	    	 
//	ticks=nus*fac_us; 						//需要的节拍数 
//	told=SysTick->VAL;        				//刚进入时的计数器值
//	while(1)
//	{
//		tnow=SysTick->VAL;	
//		if(tnow!=told)
//		{	    
// 			if(tnow<told)tcnt+=told-tnow;	//这里注意一下SYSTICK是一个递减的计数器就可以了.
//			else tcnt+=reload-tnow+told;	    
//			told=tnow;
//			if(tcnt>=ticks)break;			//时间超过/等于要延迟的时间,则退出.
//		}  
//	}										    
//}  


///*-----------------------------------------
// * 函数名   : delay_ms
// * 输入参数 ：nms :要延时的ms数  
// * 输出参数 ：无
// * 返回值   ：无
// * 函数功能 ：延时n毫秒
//-----------------------------------------*/
//void delay_ms(uint32_t nms)
//{
//    uint32_t i;
//    for(i=0;i< nms;i++)delay_us(1000);	
//}

void delay_us(uint32_t nus)
{ 
	do
  {
    __NOP();
  }while(nus--);
  
}

void delay_ms(uint32_t nms)
{
	  uint32_t apm = 130*nms;
//	uint32_t apm = 16*130*nms;
    for(uint32_t i=0;i< apm;i++);	
}

