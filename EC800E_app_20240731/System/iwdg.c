#include "iwdg.h"


IWDG_HandleTypeDef hiwdg; //独立看门狗句柄

//初始化独立看门狗
void IWDG_Init()
{
	hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
  hiwdg.Init.Reload = 1156;
	hiwdg.Init.Window = 4095;
  HAL_IWDG_Init(&hiwdg) ;
}
   
//喂独立看门狗
void IWDG_Feed(void)
{   
    HAL_IWDG_Refresh(&hiwdg); 	//喂狗
}
