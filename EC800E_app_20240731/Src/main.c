#include "stm32l0xx_hal.h"
#include "sys.h"
#include "usart.h"
#include "iwdg.h"
#include "delay.h"
#include "stm_flash.h"

#include "L76K.h"
#include "M_SW.h"
#include "lis3dh_SPI.h"
#include "Bat.h"
#include "tim.h"
#include "TIM1.h"
#include "EC800.h"
#include "APE_MQTT.h"
#include "App.h"
#include "Location.h"

int num;
int main(void)
{   
	  SCB->VTOR =FLASH_BASE + 8*1024;
  //	HAL_Init();
    INTX_ENABLE();//允许中断	
	  SystemClock_Config();
	  CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);//失能滴答定时器
	  delay_ms(1000);//延时让系统电压稳定
	  
	 //初始化电池和计算一次电量
	  Bat_Config(); 
	  delay_ms(100);
	  uint16_t aver= Get_Adc_Average(0,20);
	  app->bat_bq = Adc_To_Bq(aver);
	  
	  LPUART1_Init(9600); //调试串口初始化
	  printf("enter app\r\n");
	  
	  L76K_Config();
	  Lis3dh_Init();//加速度传感器初始化
    M_Sw_Config(); //磁传感器初始化
    LPTIM1_Init(0,65535);//大约227S中断一次唤醒单片机

		App_Start();//app初始化
		TIM2_Init(50,523); //定时2，用于产生系统基准时间和传感器采集
		EC800_Config();//Cat1模块初始化
		Update_Init(); //固件更新初始化
		Sub_Powen_On(); //开始发送power
		
	
		while(1)
		{  
			 Bat_Meausure();
			 Sub_Cmd_Handle();
			 BDS_Dev_Handle();
			 A1_Cmd_hander();
			 A0_Cmd_Handle();
			 A9_Cmd_Hander();
			 C0_Cmd_Hander();
			
			 C1_Hander();
			 C2_Hander();
			 C5_C7_Hander();
			 C6_Hander();
			 Power_On_Loc_Hander();
		 
			 APE_Pub_Queue_handler(); //队列发送
			 Ape_Hander();
		   
			 Low_Power_Hander();
		}
}
