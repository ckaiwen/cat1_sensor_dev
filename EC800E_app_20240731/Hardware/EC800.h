#ifndef __EC800_H
#define __EC800_H
#include "stm32l0xx.h"
#include "usart.h"
#include <string.h>
#include "APE_MQTT.h"
#include "Location.h"

#define Power_On_Time  16000
#define Power_Off_Time  8000

typedef struct
{  
	 char imei[16]; //模块的imel码
	 char iccid[21];//iccid码
	 uint8_t err_count;
	
}Cat1_Type_Struct;

extern Cat1_Type_Struct *cat1;

#define Cat1_PWRKEY_High()   GPIOA->BSRR = GPIO_PIN_15	    //PA15置1
#define Cat1_PWRKEY_Low()    GPIOA->BRR  = GPIO_PIN_15	  //PA15置0

#define Cat1_DTR_High()      GPIOB->BSRR = GPIO_PIN_1	    //PB1置1
#define Cat1_DTR_Low()       GPIOB->BRR = GPIO_PIN_1	  //PB1置0

#define Cat1_Get_Status()    (GPIOA->IDR & GPIO_PIN_8)


void EC800_Config(void);//EC800配置
int8_t Cat1_Power_On(void);//开机
void Cat1_Power_Off(void);//关机

char* Cat1_Send(char* s,uint16_t len,char *sub_s,uint16_t out_time);//Cat1模块反馈发送数据

int8_t Cat1_AT_At(void);//AT命令,判断模块通信是否正常
int8_t Cat1_ATI(void); //获取MT固件版本信息
int8_t Cat1_AT_QSCLK(void); //休眠
int8_t Cat1_AT_QSCLKEX(void);//AT命令,启用增强型睡眠模式
int8_t Cat1_AT_ATE(void); //设置串口发送数据不回显
int8_t Cat1_AT_QPOWD(void); //关机
int8_t Cat1_AT_CGSN(void);//获取设备的IMEI唯一码
int8_t Cat1_AT_QCCID(void);//查询(U)SIM卡的集成电路卡识别码（ICCID）
int8_t Cat1_AT_CSQ(uint8_t *rssi);//查询信号强度
int8_t Cat1_AT_QNWINFO(void);//查询网络信息,返回-1为错误，返回1为电信卡，返回2为移动卡
int8_t Cat1_AT_QLTS(char *date);//获取通过网络同步的最新时间

//内部函数
static void EC800_Log_Info_And_Init(void);//打印EC800M模块信息
#endif
