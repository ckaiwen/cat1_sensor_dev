#ifndef __TXS0108_H
#define __TXS0108_H
#include "stm32l0xx.h"

#define TXS_EN()   HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_SET)	    //PB6��1
#define TXS_Dis()  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET)	 	//PB6��0

void TXS_Config(void);
#endif

