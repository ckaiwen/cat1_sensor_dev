#ifndef __L76K_H
#define __L76K_H

#include "stm32l0xx.h"
#include "usart.h"
#include <string.h>

#define L76K_On()       GPIOA->BSRR = GPIO_PIN_0;
#define L76K_Off()      GPIOA->BRR = GPIO_PIN_0;

typedef struct
{  
	 int8_t bds_get_flag ;    //bds获取到数据的标志，1为获取到，0w为未获取到
	 char bds[32];//bsd字符串
	 char date[15]; //时间 
}L76K_Type_Struct;

extern L76K_Type_Struct *L76k;


void L76K_Config(void); //L76K配置
int8_t L76K_Data_Decode(char *str);//串口接收到数据的解析
#endif

