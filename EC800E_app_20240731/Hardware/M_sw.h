#ifndef __M_SW_H
#define __M_SW_H
#include "stm32l0xx.h"

typedef struct
{  
	 uint8_t sw1;
	 uint8_t sw;
}M_Sw_Struct;

extern M_Sw_Struct *m_sw;
#define M_Sw_State_Get()   !(GPIOB->IDR & GPIO_PIN_0)  //u5
#define M_Sw_State_Get1()  !(GPIOB->IDR & GPIO_PIN_7) //u4
void M_Sw_Config(void);
#endif

