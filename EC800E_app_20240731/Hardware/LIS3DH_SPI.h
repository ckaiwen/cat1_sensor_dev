#ifndef __LIS3DH_SPI__H
#define __LIS3DH_SPI__H
#include "stm32l0xx_hal.h"
#include "delay.h"
#include "usart.h"

/*用户参数*/
#define CTRL_REG1_Config_Normal    	0x57   //100hz,正常模式,x,y,z通道都使能
#define CTRL_REG1_Config_Sleep    	0x00   //掉电模式

//寄存器地址
#define STATUS_REG_AUX    0x7
#define OUT_ADC1_L   			0x8
#define OUT_ADC1_H    		0x9
#define OUT_ADC2_L    		0x0A
#define OUT_ADC2_H    		0x0B
#define OUT_ADC3_L    		0x0C
#define OUT_ADC3_H    		0x0D
#define WHO_AM_I    			0x0F
#define CTRL_REG0    			0x1E
#define TEMP_CFG_REG    	0x1F
#define CTRL_REG1   		  0x20
#define CTRL_REG2    			0x21
#define CTRL_REG3    			0x22
#define CTRL_REG4    			0x23
#define CTRL_REG5   			0x24
#define CTRL_REG6   			0x25
#define REFERENCE   			0x26
#define STATUS_REG    		0x27
#define OUT_X_L    				0x28
#define OUT_X_H    				0x29
#define OUT_Y_L    				0x2A
#define OUT_Y_H    				0x2B
#define OUT_Z_L    				0x2C
#define OUT_Z_H    				0x2D
#define FIFO_CTRL_REG    	0x2E
#define FIFO_SRC_REG    	0x2F
#define INT1_CFG    			0x30
#define INT1_SRC    			0x31
#define INT1_THS    			0x32
#define INT1_DURATION     0x33
#define INT2_CFG    			0x34
#define INT2_SRC    			0x35
#define INT2_THS    			0x36
#define INT2_DURATION     0x37
#define CLICK_CFG    			0x38
#define CLICK_SRC    			0x39
#define CLICK_THS    			0x3A
#define TIME_LIMIT   		  0x3B
#define TIME_LATENCY   		0x3C
#define TIME_WINDOW    		0x3D
#define ACT_THS    				0x3E
#define ACT_DUR    				0x3F

#define   WHO_AM_I_RETURN 0x33 //芯片ID号

//CS的IO口
#define LI3DH_CS_GPIO   GPIOA
#define LI3DH_CS_PIN    GPIO_PIN_4

void Lis3dh_Init(void);
u8 Lis3dh_Read_WhoAmI(void);//读取芯片ID
u8 Read_CTRL_REG1(void);
u8 Write_CTRL_REG1(u8 REG1_Config);
u16 Read_OUT_X(void);
u16 Read_OUT_Y(void);
u16 Read_OUT_Z(void);
int16_t Cal_Acc(uint16_t value);
#endif


