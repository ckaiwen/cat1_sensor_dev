#include "L76K.h"


L76K_Type_Struct L76k_Struct;
L76K_Type_Struct *L76k = &L76k_Struct;

//L76K配置
void L76K_Config()
{
   GPIO_InitTypeDef GPIO_InitStruct = {0};

 //  __HAL_RCC_GPIOA_CLK_ENABLE();
  
   GPIO_InitStruct.Pin = GPIO_PIN_0;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	 GPIO_InitStruct.Speed=GPIO_SPEED_LOW; 		//低速
   HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	 L76K_Off() ;
}

//串口接收到数据的解析
int8_t L76K_Data_Decode(char *str)
{  
	char *str1;
	uint8_t idx=0;
	
	L76k->date[8]=str[7];//小时高位
	L76k->date[9]=str[8];//小时低位
	L76k->date[10]=str[9];//分低位
	L76k->date[11]=str[10];//分低位
	L76k->date[12]=str[11];//秒高位
	L76k->date[13]=str[12];//秒低位
	
  str1 = strstr(str,"0,A");
	if(str1==NULL){
	  return -1;
	}
	
	for(uint8_t i=0;i<10;i++)
	{ 
		if(i==2){  //小数点前移两位
		  L76k->bds[idx++] = '.';
		}else if(i==4){
	    continue;
	 }
		L76k->bds[idx++]= *(str1+4+i);
	}
	
	L76k->bds[idx++]=':', //分开符号
	
	str1 = strstr(str1,"N,");
  if(str1==NULL){
	  return -1;
	}
	for(uint8_t i=0;i<11;i++)
	{ 
		if(i==3)  //小数点前移两位
		{
		  L76k->bds[idx++] = '.';
		}
		else if(i==5)
   {
	    continue;
	 }
		L76k->bds[idx++]= *(str1+2+i);
	}
	L76k->bds[idx]=0x00;
	
	str1 = strstr(str,",,,A,V");
	if(str1==NULL){
	   return -1;
	}
	L76k->date[0]='2';
	L76k->date[1]='0';
	L76k->date[2]=*(str1-2);
	L76k->date[3]=*(str1-1);
	L76k->date[4]=*(str1-4);
	L76k->date[5]=*(str1-3);
	L76k->date[6]=*(str1-6);
	L76k->date[7]=*(str1-5);
	L76k->date[14]='\0';
	return 0;
}



