#include "Bat.h"

ADC_HandleTypeDef hadc;

void Bat_Config()
{
  GPIO_InitTypeDef GPIO_Initure;
  __HAL_RCC_ADC1_CLK_ENABLE();            //使能ADC1时钟
  __HAL_RCC_GPIOA_CLK_ENABLE();			      //开启GPIOA时钟
	
  GPIO_Initure.Pin=GPIO_PIN_1;            //PA1
  GPIO_Initure.Mode=GPIO_MODE_ANALOG;     //模拟
  GPIO_Initure.Pull=GPIO_NOPULL;          //不带上下拉
  HAL_GPIO_Init(GPIOA,&GPIO_Initure);
	
	hadc.Instance = ADC1;
	hadc.Init.OversamplingMode = DISABLE;
	hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
	hadc.Init.Resolution = ADC_RESOLUTION_12B;
	hadc.Init.SamplingTime = ADC_SAMPLETIME_160CYCLES_5;
	hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
	hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc.Init.ContinuousConvMode = DISABLE;
	hadc.Init.DiscontinuousConvMode = DISABLE;
	hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc.Init.DMAContinuousRequests = DISABLE;
	hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	hadc.Init.LowPowerAutoWait = DISABLE;
	hadc.Init.LowPowerFrequencyMode = DISABLE;
	hadc.Init.LowPowerAutoPowerOff = DISABLE;                      
	HAL_ADC_Init(&hadc);                                 //初始化 
	
	HAL_ADCEx_Calibration_Start(&hadc,ADC_SINGLE_ENDED);					 //校准ADC
	
	ADC_ChannelConfTypeDef sConfig = {0};
	sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  HAL_ADC_ConfigChannel(&hadc, &sConfig);
		
}


//获取指定通道的转换值，取times次,然后平均 
//times:获取次数
//返回值:通道ch的times次转换结果平均值

u16 Get_Adc_Average(u32 ch,u8 times)
{
	u32 sum=0;
	uint16_t current_adc;
	 __HAL_RCC_ADC1_CLK_ENABLE();            //使能ADC1时钟
	
	for(uint8_t t=0;t<times;t++)
	{ 
		delay_ms(5);
		HAL_ADC_Start(&hadc);   
		HAL_ADC_PollForConversion(&hadc,50);                //轮询转换
    current_adc = HAL_ADC_GetValue(&hadc);
		sum += current_adc;
		
	}
	
	HAL_ADC_Stop(&hadc);
  __HAL_RCC_ADC1_CLK_DISABLE();	
	return sum/times;
} 

uint8_t Adc_To_Bq(float adc_value)
{  
	 #define offset 1.8*2.0/4096
  // float bat_volt = adc_value*1.8*2.0/4096.0-0.05;
	 float bat_volt = adc_value*0.00087891;
 //   int16_t level = (bat_volt- Bat_Min_Volt)*100/(Bat_Max_Volt -Bat_Min_Volt);
	 uint8_t level = (bat_volt- Bat_Min_Volt)*62.5;
   if(level>100)
   {
      level= 100;
   }
   else if(level<1)
   {
      level =1;
   }
   return level;
}




