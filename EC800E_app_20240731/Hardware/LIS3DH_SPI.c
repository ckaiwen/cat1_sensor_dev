#include "Lis3dh_SPI.h"

SPI_HandleTypeDef hspi1;
/*Lis3dh初始化*/
void Lis3dh_Init()
{
  
	 GPIO_InitTypeDef GPIO_InitStruct = {0};

		__HAL_RCC_SPI1_CLK_ENABLE();
	//   __HAL_RCC_GPIOA_CLK_ENABLE();

		GPIO_InitStruct.Pin = GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF0_SPI1;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    
	 //CS管脚
	GPIO_InitStruct.Pin= LI3DH_CS_PIN;//PA4为CS
	GPIO_InitStruct.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
	GPIO_InitStruct.Pull=GPIO_PULLUP; 			//上拉
	HAL_GPIO_Init(LI3DH_CS_GPIO,&GPIO_InitStruct);
	HAL_GPIO_WritePin(LI3DH_CS_GPIO,LI3DH_CS_PIN,GPIO_PIN_SET);
	 	 
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  HAL_SPI_Init(&hspi1);
  
	
	 delay_ms(100);
	 while(1){
	    if(Lis3dh_Read_WhoAmI()== WHO_AM_I_RETURN){
			   break;
			}
			//printf("tip:%s","Lis3dh Init Fail!\r\n");
			delay_ms(2000);
	 }
}


u8 Lis3dh_SPI_Read(u8 reg)
{
    u8 sendAddr;
	  u8 readData;
	  
	  sendAddr = reg|0x80;
		HAL_GPIO_WritePin(LI3DH_CS_GPIO,LI3DH_CS_PIN,GPIO_PIN_RESET);
		delay_us(1);
    HAL_SPI_Transmit(&hspi1, &sendAddr, 1, 1000);
    HAL_SPI_Receive(&hspi1, &readData, 1, 1000);
		delay_us(1);
		HAL_GPIO_WritePin(LI3DH_CS_GPIO,LI3DH_CS_PIN,GPIO_PIN_SET);
    return readData;
}


//Lis3dh_SPI写函数
void Lis3dh_SPI_Write(u8 reg,u8 data)
{	  
	  u8  sendAddr;
	  u8  sendData;
	  sendAddr = reg&0x7f;
	  sendData = data;
		HAL_GPIO_WritePin(LI3DH_CS_GPIO,LI3DH_CS_PIN,GPIO_PIN_RESET);
		delay_us(1);
	  HAL_SPI_Transmit(&hspi1, &sendAddr, 1, 1000);
    HAL_SPI_Transmit(&hspi1, &sendData, 1, 1000);
		delay_us(1);
		HAL_GPIO_WritePin(LI3DH_CS_GPIO,LI3DH_CS_PIN,GPIO_PIN_SET);
}


/*读取芯片ID*/
u8 Lis3dh_Read_WhoAmI(void)
{
	return Lis3dh_SPI_Read(WHO_AM_I);
}

u8 Read_CTRL_REG1(void)
{
  return Lis3dh_SPI_Read(CTRL_REG1);
}

u8 Write_CTRL_REG1(u8 reg1_config)
{
   Lis3dh_SPI_Write(CTRL_REG1,reg1_config);
	 return 0;
}

/**************************************************************************
Name: u16 Read_OUT_X(void)
Function: 查看OUT_X寄存器的值
Description: 
Input: 
Output: 
Return: 寄存器值
Others: 
**************************************************************************/ 
u16 Read_OUT_X(void)
{
	return (Lis3dh_SPI_Read(OUT_X_H)<<8 | Lis3dh_SPI_Read(OUT_X_L));
}

/**************************************************************************
Name: u16 Read_OUT_Y(void)
Function: 查看OUT_Y寄存器的值
Description: 
Input: 
Output: 
Return: 寄存器值
Others: 
**************************************************************************/ 
u16 Read_OUT_Y(void)
{
	return (Lis3dh_SPI_Read(OUT_Y_H)<<8 | Lis3dh_SPI_Read(OUT_Y_L));
}

/**************************************************************************
Name: u16 Read_OUT_Z(void)
Function: 查看OUT_Z寄存器的值
Description: 
Input: 
Output: 
Return: 寄存器值
Others: 
**************************************************************************/ 
u16 Read_OUT_Z(void)
{
	return (Lis3dh_SPI_Read(OUT_Z_H)<<8 | Lis3dh_SPI_Read(OUT_Z_L));
}

int16_t Cal_Acc(uint16_t value)
{ 
	value = value>>4;
	if((value&0x0800)==0) //整数
	{
		return value;
	}
	
	int16_t temp =~value;
	temp = temp+1;
	temp = -(temp&0x0fff);
	return temp;
}

