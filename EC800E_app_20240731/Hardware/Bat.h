#ifndef __BAT_H
#define __BAT_H

#include "stm32l0xx_hal.h"
#include "sys.h"
#include "delay.h"

#define Bat_Max_Volt 3.6  //最大电压
#define Bat_Min_Volt 2.0    //最小电压

void Bat_Config(void);
u16 Get_Adc_Average(u32 ch,u8 times);
uint8_t Adc_To_Bq(float adc_value);
#endif



