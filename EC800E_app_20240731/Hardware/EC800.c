#include "EC800.h"

Cat1_Type_Struct cat1_type;
Cat1_Type_Struct *cat1 = &cat1_type;

void Cat1_PWR_On()
{
    GPIO_InitTypeDef GPIO_Initure;
	  GPIO_Initure.Mode=GPIO_MODE_OUTPUT_OD;  //浮空输出
	  GPIO_Initure.Speed=GPIO_SPEED_LOW; 		//低速
	  GPIO_Initure.Pin=GPIO_PIN_6;  //PB6
		HAL_GPIO_Init(GPIOB,&GPIO_Initure);
	
	 // HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET);
	  GPIOB->BRR = GPIO_PIN_6;
}

void Cat1_PWR_Off()
{
    GPIO_InitTypeDef GPIO_Initure;
		GPIO_Initure.Mode=GPIO_MODE_INPUT;  //输入
		GPIO_Initure.Pull=GPIO_NOPULL; 			//没上下拉
		GPIO_Initure.Speed=GPIO_SPEED_LOW; 		//低速
	  GPIO_Initure.Pin=GPIO_PIN_6;  //PB6
	  HAL_GPIO_Init(GPIOB,&GPIO_Initure);
}

void EC800_Config()
{
		GPIO_InitTypeDef GPIO_Initure;
    
//	  __HAL_RCC_GPIOA_CLK_ENABLE(); //开启 GPIOA 时钟
//		__HAL_RCC_GPIOB_CLK_ENABLE(); //开启 GPIOB 时钟
	
	  GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
	  GPIO_Initure.Speed=GPIO_SPEED_LOW; 		//低速
		
	  //PWRKEY，在开机状态下拉低PWRKEY至少650ms后释放关机
	  GPIO_Initure.Pin=GPIO_PIN_15;  //PA15
		HAL_GPIO_Init(GPIOA,&GPIO_Initure);
    
	  //DTR,通过主机拉低模块 MAIN_DTR可唤醒模块
	  GPIO_Initure.Pin=GPIO_PIN_1;  //PB1
		HAL_GPIO_Init(GPIOB,&GPIO_Initure);
	  
//	 //Power,低电平开机，高电平关机
//	  GPIO_Initure.Pin=GPIO_PIN_6;  //PB6
//		HAL_GPIO_Init(GPIOB,&GPIO_Initure);
	
	  //输入
		GPIO_Initure.Mode=GPIO_MODE_INPUT;  //输入
		GPIO_Initure.Pull=GPIO_NOPULL; 			//没上下拉
		GPIO_Initure.Speed=GPIO_SPEED_LOW; 		//低速
		
		//STATUS,当模块正常开机时，STATUS 输出高电平
	  GPIO_Initure.Pin=GPIO_PIN_8;  //PA8
		HAL_GPIO_Init(GPIOA,&GPIO_Initure);

		Cat1_DTR_High();
		Cat1_PWRKEY_High();
	  Cat1_PWR_Off();
		delay_ms(1000);
		 
	  EC800_Log_Info_And_Init();
}


/*打印EC800模块信息和初始化*/
void EC800_Log_Info_And_Init()
{
  //获取imel
	 while(1)
	 {
	     if(Cat1_AT_CGSN()==0){
			    break;
			 }
			 delay_ms(3000);
	 }
	 
	 //获取sim卡uuid
	 while(1)
	 {
	     if(Cat1_AT_QCCID()==0){
			    break;
			 }
			 delay_ms(3000);
	 }
	 while(1){
	   if(Loc_AT_Set_Token()==0){
		    break;
		 }
		 delay_ms(3000);
	 }
	
}

/*开机*/
int8_t Cat1_Power_On()
{  
   if(Cat1_Get_Status()!=GPIO_PIN_RESET){ //已经在开机中  
	    return 1;
	 }
	  printf("cat start poweron\r\n");
	 //开机初始化
	 Cat1_PWRKEY_Low();
	 Cat1_PWR_On(); 
	 USART2_Init(9600);
	 com2->rx_len=0;
	 
	 //判断开机信号
	 uint16_t wait_time=0;
	 while(wait_time<= Power_On_Time )
	 { 
		 //if(strstr((char*)com2->rx_buf,"RDY")!=NULL || Cat1_Get_Status()!=GPIO_PIN_RESET){ //status状态已经在开机中
		 if(strstr((char*)com2->rx_buf,"RDY")!=NULL){
			   printf("cat poweron\r\n");
 			   delay_ms(2000); 
			   Cat1_AT_QSCLK();
			   loc->need_get_loc=1;
				 return 0;  
		 }
		 delay_ms(1);
		 wait_time+=1;
	 }
	 
	USART2_DeInit();
  Cat1_PWRKEY_High();
	Cat1_PWR_Off(); 
	printf("cat poweron fail\r\n");	
	delay_ms(500); 
	return -1;		 
}

/*关机,已经关机中返回1，开机成功返回0,关机失败返回-1*/
void Cat1_Power_Off()
{  
	 ape->conect_flag=0;
   if(Cat1_Get_Status()==GPIO_PIN_RESET){ //已经在关机中  	
	    return ;
	 }
	// Mqtt_QMTDISC();
	 Cat1_Send("AT+QMTDISC=0\r\n",14,"QMTDISC",3000);//减少代码大小
	 delay_ms(100);
	// Cat1_AT_QPOWD();
    Cat1_Send("AT+QPOWD=0\r\n",12,"DOWN",5000);//减少代码大小
  
   USART2_DeInit();
	 printf("cat poweroff\r\n");		 
	 delay_ms(Power_Off_Time);
   Cat1_PWR_Off();
	 delay_ms(1000);
	 return ;
}


/*Cat1模块反馈发送
 *buf为发送的内容
 *len为发送的长度
 *out_time为等待返回的超时时间
*/
char* Cat1_Send(char* s,uint16_t len,char *sub_s,uint16_t out_time)
{    
	   char *recv=NULL; //接收到的字符串
	   uint16_t have_rec_time=0; //已经接收时间
	   
	   //开机
	   if(Cat1_Power_On()==-1){ //开机失败
		    return NULL;
		 }
		  //唤醒
	   Cat1_DTR_Low();
	   delay_ms(10);
	   
		 //发送
		 Usart2Send((uint8_t*)s,len);
	   com2->rx_len=0;
		 printf("send:%s",(char*)s);	
	    
		 //等待接收完成
		 while(1)
		 {  
			  if(have_rec_time < out_time){
					 if(com2->rx_len>=6 && com2->rx_buf[com2->rx_len-2]==0x0D && com2->rx_buf[com2->rx_len-1]==0x0A){
						  if(strstr((char*)com2->rx_buf,sub_s)!=NULL){   //接收到正确的字符串
						     recv=(char*)com2->rx_buf;
						     break;
							}else if(strstr((char*)com2->rx_buf,"ERROR")!=NULL){   //接收到错误的字符串
						     break;
							}
					 }
				}
				else {   
				    break;
				}
				delay_ms(1);
				have_rec_time+=1;
		 }
		 
		 //打印数据
		   uint8_t idx=0;
		 if(com2->rx_len>=4){
 			 if(com2->rx_buf[0]==0x0D && com2->rx_buf[1]==0x0A){
			    idx=2;
			 }
       printf("recv:%s\r\n",(char*)&com2->rx_buf[idx]);		 
		 }
		 
		 //记录错误次数
		 if(recv==NULL){
		    cat1->err_count+=1;
		 }else{
		   cat1->err_count=0;
		 }
		 
		  //错误次数过多，关机重启
	   if(cat1->err_count >=Cat1_Max_Err_Count){
			  cat1->err_count=0;
		    Cat1_Power_Off();
			 	delay_ms(200);
		 }
		 
		 Cat1_DTR_High(); //拉高进入休眠
		 return recv;
}


//AT命令,判断模块通信是否正常
int8_t Cat1_AT_At()
{  
   if(Cat1_Send("AT\r\n",4,"OK",300)==NULL) {
	     return -1;
	 }
	 return 0;
}


//AT命令,获取MT固件版本信息
int8_t Cat1_ATI()
{  
   if(Cat1_Send("ATI\r\n",5,"OK",300)==NULL) {
	     return -1;
	 }
	 return 0;
}


//AT命令,启用睡眠模式
int8_t Cat1_AT_QSCLK()
{  
   if(Cat1_Send("AT+QSCLK=1\r\n",12,"OK",300)==NULL) {
	     return -1;
	 }
	 return 0;
}

//AT命令,启用增强型睡眠模式
int8_t Cat1_AT_QSCLKEX()
{  
   if(Cat1_Send("AT+QSCLKEX=1,1,10\r\n",19,"OK",300)==NULL) {
	     return -1;
	 }
	 return 0;
}



//立即关机命令
int8_t Cat1_AT_QPOWD()
{  
   if(Cat1_Send("AT+QPOWD=0\r\n",12,"DOWN",5000)==NULL) {
	     return -1;
	 }
	 return 0;
}

//获取设备的IMEI唯一码
int8_t Cat1_AT_CGSN()
{
   char* str= Cat1_Send("AT+GSN\r\n",8,"OK",300);
	 if(str == NULL || strlen(str) < 20)
	 {
	     return -1;
	 }
	 for(uint8_t i=0;i<15;i++){
	   cat1->imei[i]=*(str+2+i);
	 }
	 cat1->imei[15]='\0';
	
	 return 0;
}

//查询(U)SIM卡的集成电路卡识别码（ICCID）
int8_t Cat1_AT_QCCID()
{
   char *str=Cat1_Send("AT+QCCID\r\n",10,"OK",300);
	 if(str==NULL){
	     return -1;
	 }
	 
	 str= strstr(str,"ID:");
	 uint8_t len=strlen(str);
	 uint8_t count=0;
	 for(uint8_t i=0;i<len;i++){
	    if(str[i]>=0x30 && str[i]<=0x39){
			   cat1->iccid[count]=str[i];
				 count+=1;
			}
	 }
	 return 0;
}

//查询信号强度
int8_t Cat1_AT_CSQ(uint8_t *rssi)
{
   char* str= Cat1_Send("AT+CSQ\r\n",8,"OK",300);
	 if(str == NULL){
	     return -1;
	 }
	 str=strstr(str,"CSQ:");
	 if(str==NULL || strlen(str)<10){
	    return -1;
	 }
	 *rssi = (str[5]-0x30)*10 + str[6]-0x30;
	 return 0;
}

//查询网络信息
//返回-1为错误，返回1为电信卡，返回2为移动卡
int8_t Cat1_AT_QNWINFO()
{
   char* str= Cat1_Send("AT+QNWINFO\r\n",12,"OK",300);
	 if(str == NULL || strstr(str,"NO SERVICE")){
	     return -1;
	 }
	 
	 if(strstr(str,"FDD LTE")){
	    return 1;
	 }else{
		  return 2;
	 }
}

//获取通过网络同步的最新时间
//返回为时间字符串
//0查询通过网络同步的最新时间
//1查询通过网络同步的最新时间计算出的当前 GMT 时间
//2查询通过网络同步的最新时间计算出的当前本地时间
//date的内存大小需要大于等于20
int8_t Cat1_AT_QLTS(char *date)
{
   char* str= Cat1_Send("AT+QLTS=2\r\n",11,"OK",300);
	 if(str == NULL || strstr(str,"2070")!=NULL|| strlen(str) < 40)
	 {
	     return -1;
	 }
	 
	 str = strstr(str,"\"2");
	 if(str==NULL || strlen(str)<22)
	 {
	   return -1;
	 }
	 
	 str=str+1;
	 for(uint8_t i=0;i<19;i++){  
		  
	    if(*str>=0x30 && *str<=0x39){
			   *date= *str;
				 date+=1;
			}
			str+=1;
	 }
	 
	 return 0;
}




