#include "M_SW.h"

M_Sw_Struct m_sw_struct;
M_Sw_Struct *m_sw=&m_sw_struct;

void M_Sw_Config()
{
   GPIO_InitTypeDef GPIO_InitStruct = {0};
   
   __HAL_RCC_GPIOB_CLK_ENABLE();

  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
	
	GPIO_InitStruct.Pin = GPIO_PIN_0;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	 
	GPIO_InitStruct.Pin = GPIO_PIN_7;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
  
	m_sw->sw =M_Sw_State_Get();
	m_sw->sw1 =M_Sw_State_Get1();
}

