
app生成方式:
1.main函数中使用该配置
SCB->VTOR =FLASH_BASE + 8*1024;
2.Options for Target->output里面配置导出bin文件的名称
3.Options for Target->Linker R/O BAse里面值改为0x08002000
5.Options for Target->Linker Scatter file清空
6.点击build进行编译
7.在文件夹中可以找到编译好的bin文件


更新日志：
20240619
1.CAT1模组：EC800E

20240627
1.上报磁开关状态及编号

20240708
1.u4磁开关误报，只用u5工作，删除u4，让其不工作

20240731
1.更换磁铁
2.C2上报成功1次后，30分钟后如果还有触发，则不再上报
3.将C2的阀值设为